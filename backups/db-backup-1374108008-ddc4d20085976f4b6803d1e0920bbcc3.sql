DROP TABLE clients;

CREATE TABLE `clients` (
  `ClientName` varchar(80) COLLATE utf8_bin NOT NULL,
  `ClientID` varchar(80) COLLATE utf8_bin NOT NULL,
  `KeyCode` varchar(80) COLLATE utf8_bin NOT NULL,
  `Address` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `City` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `State` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `Zip` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `WorkPh` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `CellPh` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `FaxPh` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `POC` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `Email` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `FTPUrl` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `FTPUser` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `FTPPass` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `Notes` varchar(2000) COLLATE utf8_bin DEFAULT NULL,
  `NumProjects` int(11) DEFAULT NULL,
  `RecNo` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`RecNo`),
  UNIQUE KEY `ClientID` (`ClientID`),
  KEY `ClientName` (`ClientName`),
  KEY `ClientID_2` (`ClientID`),
  KEY `KeyCode` (`KeyCode`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO clients VALUES("Digital Dreams","000001","Di2.NMYZ38kyU","1117 Mariposa Dr.","Yuba City","CA","95991","","530.990.0020","","Tom Wiseman","","","","","Turning the ordinary into the extraordinary.","","1");
INSERT INTO clients VALUES("A Top Driver","ATOP-99","Di2.NMYZ38kyU","2525 Dominic Dr # H","Chico","CA","","","","","Hutch Hutchinson","","ftp.atopdriverschool.com","atopdriv","At00--9reL","","1","2");
INSERT INTO clients VALUES("Acy Brown","AB-100","Di2.NMYZ38kyU","","Gridley","CA","","","","","Acy Brown","acybrown87@gmail.com","https://www.homestead.com/~site/Login/index.ffhtml","acybrown87@gmail.com","acyjulian","","1","3");
INSERT INTO clients VALUES("Lloyd Leighton","LL-101","Di2.NMYZ38kyU","1212 Highland Ave.","Yuba City","CA","95991","","","","Lloyd Leighton","","","","","","1","4");
INSERT INTO clients VALUES("Placer Foreclosure","PF-105","Di2.NMYZ38kyU","12190 Herdal Drive, Suite 9","Auburn","CA","95603","","","","Shannon Winford","","www.placerforeclosure.com","placer","hungryhome2012","","1","5");
INSERT INTO clients VALUES("Flynn, Kossick & Associates, Inc.","FKA-106","Di2.NMYZ38kyU","11719 Prospect Hill Drive","Gold River","CA","95670","","916-712-3545","","Jeff Kossick","","www.fkanda.net","fkanda","FK$vssc1","","2","6");
INSERT INTO clients VALUES("The Musical Touch","MT-107","Di2.NMYZ38kyU","950 Lincoln Road.","Yuba City","CA","95991","","","","Mi Hui Eytcheson","","ftp.themusicaltouch.com","them0240","Studio96!","","1","7");
INSERT INTO clients VALUES("Outlaw Grafix","OG-108","Di2.NMYZ38kyU","3360 Industrial Drive","Yuba City","CA","95993","","","","Steve Oswald","","ftp.outlawgrafix.com","sogrfxco1","2004.Fatboy","","3","8");
INSERT INTO clients VALUES("Clingan Karate","CK-110","Di2.NMYZ38kyU","623 Plumas Street","Yuba City","CA","95991","","","","Charlie Clingan","","ftp.03575c9.netsolhost.com","clinganwebmaster","Frpj35a2@","Free-be Website","1","9");
INSERT INTO clients VALUES("Top This","TT-111","Di2.NMYZ38kyU","533 East Main St.","Grass Valley","CA","95945","","","","Chuck Kavros","","ftp.TopThis.biz","topthis","@dmin4web","","1","10");
INSERT INTO clients VALUES("Hope For Foundation","HF-112","Di2.NMYZ38kyU","PO Box 991807","Redding","CA","96099-1807","","530.605.5111","","Ken Knowles","","","","","","1","11");
INSERT INTO clients VALUES("Age Wise Relocation","AWR-115","Di2.NMYZ38kyU","595 Roberts St.","Reno","CA","89502","","775-233-1049","","Sharon Allen","","ftp.agewiserelocation.com","agewise123","Fuzzbee123!","","1","12");
INSERT INTO clients VALUES("St. Andrew Presbyterian","SA-116","Di2.NMYZ38kyU","1390 Franklin Road","Yuba City","CA","95993","","","","Donna Smith","","standrewpcusa.org","standrew","bSbkWaJ","Free-be Website","1","13");
INSERT INTO clients VALUES("Phillip A. Cooke","PC-117","Di2.NMYZ38kyU","1215 Plumas Street, Suite 1800","Yuba City","CA","95991","","","","Phil Cooke","","cookelawoffice.com","cookelaw","SF3C8Svz","","1","14");
INSERT INTO clients VALUES("Sportsmans Jerky","SJ-118","Di2.NMYZ38kyU","1912 State Hwy 65 Suite 170","Wheatland","CA","95662","","","","Sarah Reese","","ftp.sportsmansjerkycompany.com","jerkywebmaster","Frpj35a1!","","3","15");
INSERT INTO clients VALUES("Creative Kids Discovery Center","CK-120","Di2.NMYZ38kyU","1060 Lincoln Road","Yuba City","CA","95991","","","","Michelle Lightle","","","","","","2","16");
INSERT INTO clients VALUES("Travelers Inn","TI-122","Di2.NMYZ38kyU","215 Seventh St.","Williams","CA","95987","","","","Manish Patel","","209.90.77.12","mpatel","96QvW2C8","","1","17");
INSERT INTO clients VALUES("Total Foreclosure","TF-123","Di2.NMYZ38kyU","10556 Combie Rd.","Auburn","CA","95602","","","","Mike Ecker","","03690be.netsolhost.com","tfswebmaster","frpj35a1!","","1","18");
INSERT INTO clients VALUES("Roses To Rodeo","RR-126","Di2.NMYZ38kyU","","Live Oak","CA","95953","","","","Michelle Swift","","http://rosestorodeo.com/","","","","1","19");
INSERT INTO clients VALUES("Bev Root","BR-127","Di2.NMYZ38kyU","777 Ainsley Avenue","Yuba City","CA","95991","","","","Bev Root","","","","","","1","20");
INSERT INTO clients VALUES("Vans Bicycle Center","VB-128","Di2.NMYZ38kyU","622 Gray Ave","Yuba City","CA","95991","","","","Glenn","","","","","","2","21");
INSERT INTO clients VALUES("S.O.I. Protective","SOI-129","Di2.NMYZ38kyU","","Yuba City","CA","95991","","","","Jason Littlepage","","","","","","1","22");
INSERT INTO clients VALUES("JiggyBits","JB-130","Di2.NMYZ38kyU","","Plumas Lake","CA","95961","","530-635-7023","","Irene Mason","","03761f4.netsolhost.com","jiggybitsadmin","Frpj35a2@","","1","23");
INSERT INTO clients VALUES("CitrusTwist Kits","CT-131","Di2.NMYZ38kyU","28012 NE 151st Place","Duvall","CA","98019","","","","Trina Craig","","174.121.34.194","citadmin","lHWF%25G8+Ri=f","","1","24");
INSERT INTO clients VALUES("Caseys Pest Control","CPC-133","Di2.NMYZ38kyU","9559 Quail Meadow Ln.","Oregon House","CA","95962","","530-692-8993","","Travis Casey","travismcasey@gmail.com","0378116.netsolhost.com","caseyswebadmin","Frpj35a1!","","1","25");



DROP TABLE invoices;

CREATE TABLE `invoices` (
  `ClientName` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Company Name',
  `KeyCode` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Job` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Project` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Status` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Invoice` int(11) DEFAULT NULL COMMENT 'Invoice #',
  `DOI` varchar(14) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'Date of Invoice',
  `ProjectStart` varchar(14) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ProjectEnd` varchar(14) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Contract` tinyint(1) NOT NULL,
  `DueDate` varchar(14) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `AmountQUOTE` decimal(10,2) DEFAULT NULL,
  `AmountREC` decimal(10,2) DEFAULT NULL,
  `Qty1` int(11) DEFAULT NULL,
  `UnitPrice1` decimal(10,2) DEFAULT NULL,
  `JobDesc1` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Qty2` int(11) DEFAULT NULL,
  `UnitPrice2` decimal(10,2) DEFAULT NULL,
  `JobDesc2` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Qty3` int(11) DEFAULT NULL,
  `UnitPrice3` decimal(10,2) DEFAULT NULL,
  `JobDesc3` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Qty4` int(11) DEFAULT NULL,
  `UnitPrice4` decimal(10,2) DEFAULT NULL,
  `JobDesc4` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Qty5` int(11) DEFAULT NULL,
  `UnitPrice5` decimal(10,2) DEFAULT NULL,
  `JobDesc5` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Qty6` int(11) DEFAULT NULL,
  `UnitPrice6` decimal(10,2) DEFAULT NULL,
  `JobDesc6` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `OtherUrl` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `OtherUser` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `OtherPass` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Notes` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `RecNo` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`RecNo`),
  KEY `ClientName` (`ClientName`),
  KEY `Invoice` (`Invoice`),
  KEY `KeyCode` (`KeyCode`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

INSERT INTO invoices VALUES("A Top Driver","Di2.NMYZ38kyU","Website Design","Basic","Closed","99","6/30/2011","","","0","","250.00","250.00","1","250.00","Silver Website Package","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","1");
INSERT INTO invoices VALUES("Acy Brown","Di2.NMYZ38kyU","Website Design","Basic","Closed","100","7/9/2011","","","0","","250.00","250.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","2");
INSERT INTO invoices VALUES("Lloyd Leighton","Di2.NMYZ38kyU","Website Design","Basic","Closed","101","8/15/2011","","","0","","400.00","400.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","3");
INSERT INTO invoices VALUES("Placer Foreclosure","Di2.NMYZ38kyU","Website Design","Basic","Closed","105","8/15/2012","","","0","","675.00","675.00","1","475.00","Platinum Website Package","1","150.00","Flash Animation","1","50.00","PDF document conversion","0","0.00","","0","0.00","","0","0.00","","","","","","4");
INSERT INTO invoices VALUES("Flynn, Kossick","Di2.NMYZ38kyU","Site Modifications","Basic","Closed","106","11/2/2011","","","0","","700.00","700.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","5");
INSERT INTO invoices VALUES("The Musical Touch","Di2.NMYZ38kyU","Website Design","Basic","Closed","107","2/20/2012","","","0","","360.00","360.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","6");
INSERT INTO invoices VALUES("Outlaw Grafix","Di2.NMYZ38kyU","Website Design","Basic","Closed","108","2/19/2012","","","0","","237.50","237.50","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","7");
INSERT INTO invoices VALUES("Clingan Karate","Di2.NMYZ38kyU","Website Design","Basic","Closed","110","3/15/2012","","","0","","0.00","0.00","1","0.00","Free-bee Website","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","8");
INSERT INTO invoices VALUES("Top This","Di2.NMYZ38kyU","Website Design","Responsive","Waiting on Client","111","4/1/2012","","","0","","800.00","800.00","1","725.00","Platinum Web Design Package","2","35.00","Additional Web Pages","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","9");
INSERT INTO invoices VALUES("Hope for Foundation","Di2.NMYZ38kyU","Website Design","Basic","Closed","112","4/27/2012","","","0","","500.00","500.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","10");
INSERT INTO invoices VALUES("Outlaw Grafix","Di2.NMYZ38kyU","Site Management","Basic","Closed","114","6/7/2012","","","0","","50.00","50.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","11");
INSERT INTO invoices VALUES("Age Wise Relocation","Di2.NMYZ38kyU","Website Design","Basic","Closed","115","6/14/2012","","","0","","475.00","475.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","12");
INSERT INTO invoices VALUES("St. Andrew Presbyterian","Di2.NMYZ38kyU","Website Design","Basic","Closed","116","6/19/2012","","","0","","0.00","0.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","13");
INSERT INTO invoices VALUES("Outlaw Grafix","Di2.NMYZ38kyU","Site Modifications","Basic","Closed","119","8/20/2012","","","0","","140.00","140.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","14");
INSERT INTO invoices VALUES("Sportsmans Jerky","Di2.NMYZ38kyU","Website Design","Basic","Closed","118","9/7/2012","","","0","","470.00","470.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","PayPal.com","sarree5@aol.com","sterling8","","15");
INSERT INTO invoices VALUES("Phillip A. Cooke","Di2.NMYZ38kyU","Website Design","Basic","Waiting on Client","117","","","","0","","237.50","0.00","1","475.00","Silver Web Design package","1","-237.50","1/2 Off Design Cost Discount","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","16");
INSERT INTO invoices VALUES("Creative Kids","Di2.NMYZ38kyU","Website Design","Basic","Closed","120","12/23/2012","","","0","","535.00","535.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","godaddy.com","39927411","Creative3715","","17");
INSERT INTO invoices VALUES("Flynn, Kossick","Di2.NMYZ38kyU","Hosting Services","Basic","Closed","121","8/10/2012","","","0","","125.00","125.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","18");
INSERT INTO invoices VALUES("Travelers Inn","Di2.NMYZ38kyU","Website Design","Basic","Closed","122","3-5-2013","","","0","","810.00","810.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","http://williamstravelersinn.com/wp-admin","Manny","travelerS!","","19");
INSERT INTO invoices VALUES("Total Foreclosure","Di2.NMYZ38kyU","Hosting Services","Basic","Closed","123","4/16/2013","","","0","","160.00","160.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","https://www.networksolutions.com/manage-it/index.jsp","tfs.2012","f-trustee","","20");
INSERT INTO invoices VALUES("Outlaw Grafix","Di2.NMYZ38kyU","Site Modifications","Basic","Closed","124","11/1/2012","","","0","","50.00","50.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","21");
INSERT INTO invoices VALUES("Sportsmans Jerky","Di2.NMYZ38kyU","Site Modifications","Basic","Closed","125","11/2/2012","","","0","","50.00","50.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","PayPal.com","sarree5@aol.com","sterling8","","22");
INSERT INTO invoices VALUES("Roses to Rodeo","Di2.NMYZ38kyU","Site Modifications","WordPress","Closed","126","12/27/2012","","","0","","160.00","160.00","4","40.00","Web Site Modifications","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","23");
INSERT INTO invoices VALUES("Vans Bicycles","Di2.NMYZ38kyU","Website Design","Basic","Closed","128","2/06/13","","","0","","450.00","450.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","24");
INSERT INTO invoices VALUES("Bev Root","Di2.NMYZ38kyU","Other","Basic","Closed","127","1/30/13","","","0","","40.00","40.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","25");
INSERT INTO invoices VALUES("CitrusTwist Kits","Di2.NMYZ38kyU","Website Design","Responsive","Invoice Submitted","129","","","","0","","1500.00","0.00","1","1500.00","Custom Content Management/eCommerce Website","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","http://174.121.34.194:2082/","citadmin","lHWF%25G8+Ri=f","","26");
INSERT INTO invoices VALUES("SOI Protective","Di2.NMYZ38kyU","Website Design","Responsive","Open","129","","","","0","","620.00","300.00","1","725.00","Platinum Web Design Package","3","-35.00","Discount for fewer webpages","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","27");
INSERT INTO invoices VALUES("JiggyBits","Di2.NMYZ38kyU","Website Design","Basic","Closed","130","5/2/2013","","","0","","475.00","475.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","28");
INSERT INTO invoices VALUES("Vans Bicycles","Di2.NMYZ38kyU","Site Modifications","Basic","Closed","132","4/17/2013","","","0","","50.00","50.00","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","29");
INSERT INTO invoices VALUES("Caseys Pest Control","Di2.NMYZ38kyU","Website Design","Responsive","Closed","133","6/2/2013","","","1","","725.00","725.00","1","725.00","Platinum WebDesign Package","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","","","","","30");
INSERT INTO invoices VALUES("Sportsmans Jerky","Di2.NMYZ38kyU","Site Modifications","Basic","Closed","134","5-15-2013","","","0","","50.00","50.00","1","1.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","0","0.00","","www.paypal.com","sarree5@aol.com","sterling8","","31");
INSERT INTO invoices VALUES("Creative Kids","Di2.NMYZ38kyU","Site Management","Joomla","Closed","135","05-20-2013","","","0","","50.00","50.00","1","50.00","Monthly Maintenance / Updates","1","0.00","Designed & uploaded 2013/2014 Calendar (.JPG & .PDF)","1","0.00","Modified & uploaded 4 images for sliding gallery","1","0.00","Added additional images to online gallery","0","0.00","","0","0.00","","","","","","33");



DROP TABLE users;

CREATE TABLE `users` (
  `UserName` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Password` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `KeyCode` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `RecNo` int(11) NOT NULL,
  PRIMARY KEY (`RecNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO users VALUES("twiseman","frpj35a1","Di2.NMYZ38kyU","1");
INSERT INTO users VALUES("admin","frpj35a","Di2.NMYZ38kyU","2");
INSERT INTO users VALUES("jcarius","requiem1","Di2.NMYZ38kyU","3");



