<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php 
	session_start();
	$path = './scripts/auth.php';
	require_once($path)
?> 

<head>
<link href="styles.css" rel="stylesheet" type="text/css" />
<link href="favicon.ico" rel="shortcut icon" />
<title>WebDesign - Client Invoicing System v2.0 by Digital Dreams</title>
</head>
<body>
<div class="wrapper">
	<div class="header"></div>
	<div class="login">
		<p>&nbsp;</p>
		
		<div id="menu">
			<ul>
				<li><a href="index.php" class="current">HOME</a></li>
				<?php
					if (isset($_SESSION['USER']))
					{
        				echo '<li><a href="./scripts/viewclients.php">CLIENT LIST</a></li>';
        				echo '<li><a href="./scripts/viewprojects.php">PROJECT LIST</a></li>';
						echo '<li><a href="./scripts/prepinvoice.php">INVOICE</a></li>';
						echo '<li><a href="./scripts/export.php">EXPORT</a></li>';
						echo '<li><a href="./scripts/cron_backup.php">DB BACKUP</a></li>';
						//echo '<li><a href="./scripts/setkey.php">SET KEY</a></li>';
						echo '<li><a href="logout.php">LOGOUT</a></li>';
					}
				?>
				
			</ul>
		</div>
        <div id="submenu">
			<ul>
				<?php
					if (isset($_SESSION['USER']))
					{
						// Nothing here
					}
				?>
				
			</ul>
		</div>
        <div class="clearit"></div><br />
		
        <?php 
			if (isset($_SESSION['USER']))
				echo '<div class="UserIdent">User: <span>'. strtoupper($_SESSION['USER']) .'</span><br />Key: '. $_SESSION['KEYCODE'] .'</div>';
		?>	
	  	<br /><hr><br />

		<p>The Web Design Invoicing System application and  database were designed by DIGITAL DREAMS for exclusive use by authorized clients who have purchased a license key.</p><br />
      	<p>Use of this system assumes you agree to all of the terms and conditions outlined in the "Terms and Conditions" document.</p><br />
	  	<p id="red">*Any use of or copying of any data files, in part or in whole, to include the associated database without the author's written consent and a valid key  will be considered a copyright infringement.</p><br />
        <p id="red"> Penalties may include fines up to $150,000, and/or imprisonment.</p>
		<p>&nbsp;</p>
		<p>To purchase a key, please submit a request via the author's website <a href="http://www.digitaldrms.com" target="_blank">Digital Dreams</a></p>
	  	<p>&nbsp;</p>
        <p>&nbsp;</p>
        
		<img src="images/all_rights_reserved.png" alt="All Rights Reserved, Digital Dreams" id="allrights">
        <?php
			if (!isset($_SESSION['USER']))
			{  	
				?>
				<form action="./scripts/loginuser.php" method="post" enctype="multipart/form-data" name="loginuser" id="loginuser">        
					<h3>User Login</h3>
					<label>Username: </label><input name="Username" type="text" size="18"><br /><br />
					<label>Password: </label><input name="Password" type="password" size="18">
					<br /><br />
					<input name="submit" type="submit" value="Submit" />
					<input name="reset" type="reset" value="Reset" />
				</form>
                <?php
			}
		?>
	</div>
</div>
    <div class="clearit"></div>
    <div class="footer">
        <div id="footer1">
            <h4>Copyright &copy; 2013, <a href="#">All Rights Reserved.</a></h4>
        </div>
        <div id="footer2">
            <h4>Designed by: <a href="http://www.digitaldream-designs.com" target="_blank">Digital Dreams</a></h4>
        </div>        
    </div>
</body></html>
