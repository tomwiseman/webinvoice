<!DOCTYPE html>
<head>

</head>
<?php
	session_start();
	
	$Username = $_SESSION['USER'];	
	$time = date("\a\\t g.i a", time());
	$my_t=getdate(date("U"));
	$month = sprintf("%02s", $my_t[mon]);
	$day = sprintf("%02s", $my_t[mday]);
	$ErrorDate =("$month-$day-$my_t[year]");
	
	// Write information to log file.
	$errorlog = "./logs/log_file.txt";
	$myerror = "$Username logged out of invoicing system.";
	$error = $ErrorDate.'  '.$time .': '.$myerror."\r\n";
	if (file_exists($errorlog)) {
		file_put_contents($errorlog, $error, FILE_APPEND | LOCK_EX);
	}
	else
	{
		file_put_contents($errorlog, $error);
	}
	
	// Unset all of the session variables.
	$_SESSION = array();
	// Destroy Session
	session_destroy();		
	header("Location: index.php?loggedout=yes");
?>