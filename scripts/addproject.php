<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script src="http://code.jquery.com/jquery-latest.js"></script>

	<link href="../styles.css" rel="stylesheet" type="text/css" />
	<link href="../favicon.ico" rel="shortcut icon" />
	<title>WebDesign - Client Invoicing System v2.0 by Digital Dreams</title>	
<script type="text/javascript">
	$(document).ready(function(){
		$("form").submit(function(){
			$.post('my_ajax.php', {val1: "AddProject", val2: this.id}, function (response) {
			  var url = "../index.php";    
				$(location).attr('href',url);
			});
		});
	});
</script>
</head>
<?php 
	session_start();
	$path = 'auth.php';
	require_once($path);
	if (!isset($_SESSION['USER']) || (!isset($_SESSION['KEYCODE'])))
	{
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../logout.php?unauthorized=true">';
	}
?> 

<body>
<div class="wrapper">
	<div class="header"></div>
	<div class="login">
		<p>&nbsp;</p>
		
		<div id="menu">
			<ul>
				<li><a href="../index.php">HOME</a></li>
				<?php
					if (isset($_SESSION['USER']))
					{
        				echo '<li><a href="viewclients.php">CLIENT LIST</a></li>';
        				echo '<li><a href="viewprojects.php" class="current">PROJECT LIST</a></li>';
						echo '<li><a href="prepinvoice.php">INVOICE</a></li>';
						echo '<li><a href="export.php">EXPORT</a></li>';						
					}
				?>
				<li><a href="../logout.php">LOGOUT</a></li>
			</ul>
		</div>
        <div id="submenu">
			<ul>
				<?php
					if (isset($_SESSION['USER']))
					{
						echo '<li><a href="addproject.php" class="current">ADD PROJECT</a></li>';
					}
				?>
				
			</ul>
		</div>
        <div class="clearit"></div><br />
		
        <?php echo '<div class="UserIdent">User: <span>'. strtoupper($_SESSION['USER']) .'</span></div>'; ?>	
		<br /><hr><br />
		<?php
			$my_t=getdate(date("U"));
			$month = sprintf("%02s", $my_t[mon]);
			$Date =("$month-$my_t[mday]-$my_t[year]");
		?>
		<img src="../images/content_header.png" >
		<form action="my_ajax.php" width="400" height="400"  method="post" enctype="multipart/form-data" name="add">
			<p><label>Client: </label><select name="ClientName" size="1">
            <?php
				$con = mysql_connect($database_host,$username,$password);
				if (!$con)
				{
				  die('Could not connect: ' . mysql_error());
				}
				
				mysql_select_db($database_name, $con);        
				$result = mysql_query("SELECT ClientName FROM clients ORDER BY ClientName asc");
				while($row = mysql_fetch_array($result))
       			{
					echo '<option>'.$row['ClientName'].'</option>';						
				}
				echo '</select></p>';
				
			?>
			<p><label>Job Type: </label><select name="Job">
                   <option>Website Design</option>
                   <option>Site Modifications</option>
                   <option>Site Management</option>
                   <option>Hosting Services</option>
                   <option>SEO</option>
                   <option>Analytics</option>
                   <option>PHP Scripting</option>
                   <option>ASP Scripting</option>
                   <option>Business Card</option>
                   <option>Brochure</option>     
                   <option>Other</option>
                   </select>
			<label>Project Type: </label><select name="Project">
                   <option>Basic</option>                
                   <option>Responsive</option>                
                   <option>Joomla</option>
                   <option>WordPress</option>
                   <option>Custom CMS</option>
                   <option>eCommerce</option>
                   <option>Other</option>
                   </select>
             <label>Status: </label><select name="Status">
                   <option>Open</option>                
                   <option>Testing Phase</option>                
                   <option>On Hold</option>
                   <option>Waiting on Client</option>
                   <option>Invoice Submitted</option>
                   <option>Closed</option>
                   </select></p> 
            <?php
				$con = mysql_connect($database_host,$username,$password);
				if (!$con)
				{
				  die('Could not connect: ' . mysql_error());
				}
				
				mysql_select_db($database_name, $con);        
				$result = mysql_query("SELECT Invoice FROM invoices");
				while($row = mysql_fetch_array($result))
       			{
					$LastInv = $row['Invoice'];
				}
				$LastInv++;
				echo '<p><label>Invoice: # </label><input name="Invoice" type="number" size="14" placeholder="000000" value="'. $LastInv .'" />';
				echo '<label>Start Date: </label><input name="ProjectStart" type="number" size="14" placeholder="##/##/####" value="'. $Date .'" /></p></br />';
				mysql_close($con);			
			?>
            <p><label>Contract: </label><input name="Contract" type="checkbox" value="1">
            <label>Due Date: </label><input name="DueDate" type="number" size="14" placeholder="##/##/####" /></p><br />            
            <h4>LINE ITEMS - </h4>
            <p><label>QTY: # </label><input name="Qty1" type="number" size="10" placeholder="0" value="1" />
			<label>Job Desc: </label><input name="JobDesc1" type="text" size="60" maxlength="80" placeholder="Details of Line Item">
            <label>Unit Price: $ </label><input name="UnitPrice1" type="number" size="14" placeholder="0.00" value="0.00" /></p><br />
            
            <p><label>QTY: # </label><input name="Qty2" type="number" size="10" placeholder="0" value="0" />
			<label>Job Desc: </label><input name="JobDesc2" type="text" size="60" maxlength="80" placeholder="Details of Line Item">
            <label>Unit Price: $ </label><input name="UnitPrice2" type="number" size="14" placeholder="0.00" value="0.00" /></p><br />
            
            <p><label>QTY: # </label><input name="Qty3" type="number" size="10" placeholder="0" value="0" />
            <label>Job Desc: </label><input name="JobDesc3" type="text" size="60" maxlength="80" placeholder="Details of Line Item">
            <label>Unit Price: $ </label><input name="UnitPrice3" type="number" size="14" placeholder="0.00" value="0.00" /></p><br />
            
            <p><label>QTY: # </label><input name="Qty4" type="number" size="10" placeholder="0" value="0" />
			<label>Job Desc: </label><input name="JobDesc4" type="text" size="60" maxlength="80" placeholder="Details of Line Item">
            <label>Unit Price: $ </label><input name="UnitPrice4" type="number" size="14" placeholder="0.00" value="0.00" /></p><br />
            
            <p><label>QTY: # </label><input name="Qty5" type="number" size="10" placeholder="0" value="0" />
            <label>Job Desc: </label><input name="JobDesc5" type="text" size="60" maxlength="80" placeholder="Details of Line Item">
            <label>Unit Price: $ </label><input name="UnitPrice5" type="number" size="14" placeholder="0.00" value="0.00" /></p><br />
            
            <p><label>QTY: # </label><input name="Qty6" type="number" size="10" placeholder="0" value="0" />
			<label>Job Desc: </label><input name="JobDesc6" type="text" size="60" maxlength="80" placeholder="Details of Line Item">
            <label>Unit Price: $ </label><input name="UnitPrice6" type="number" size="14" placeholder="0.00" value="0.00" /></p><br />      
                       
			<p><label>Amount Quoted: $ </label><input name="AmountQUOTE" type="number" size="14" placeholder="0.00" value="0.00" />
			<label>Amount Received: $ </label><input name="AmountREC" type="number" size="14" placeholder="0.00" value="0.00"  /></p><br />
            
			<p><label>Other Url: </label><input name="OtherUrl" type="text" size="25" maxlength="80" placeholder="Additional URL" />
			<label>Username: </label><input name="OtherUser" type="text" size="25" maxlength="80" />
			<label>Password: </label><input name="OtherPass" type="text" size="25" maxlength="80" /></p><br />
			<p><label>Notes: </label><textarea name="Notes" cols="50" rows="5" placeholder="Additional Notes"></textarea></p><br />
        	<input name="Type" type="hidden" value="AddProject">
			<p>&nbsp;</p>		
			<input name="submit" type="submit" value="Submit" />
			<input name="reset" type="reset" value="Reset" />		
        </form>
        <img src="../images/content_footer.png" >
        <p>&nbsp;</p>
	</div>
</div>
<div class="clearit"></div>
<div class="footer">
	<div id="footer1">
		<h4>Copyright &copy; 2013. <a href="#">All Rights Reserved.</a></h4>
	</div>
	<div id="footer2">
		<h4>Designed by: <a href="http://www.digitaldream-designs.com" target="_blank">Digital Dreams</a></h4>
	</div>
	
</div>
</body></html>
