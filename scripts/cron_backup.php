<?php
	$path = 'auth.php';
	require_once($path);
	$table='*';

	$time = date("\a\\t g.i a", time());
	$my_t=getdate(date("U"));
	$month = sprintf("%02s", $my_t[mon]);
	$ErrorDate =("$my_t[year]-$month-$my_t[mday]");
	
	backup_tables($database_host, $username, $password, $database_name, '*');	
	
	/* backup the db OR just a table */
	function backup_tables($host,$user,$pass,$name,$tables = '*')
	{
		$link = mysql_connect($host,$user,$pass);
		mysql_select_db($name,$link);
		
		//get all of the tables
		if($tables == '*')
		{
			$tables = array();
			$result = mysql_query('SHOW TABLES');
			//while($row = mysql_fetch_array($result))
			while($row = mysql_fetch_row($result))
			{
				$tables[] = $row[0];
				echo 'Row: '. $row[0]. '<br />';
			}
		}
		else
		{
			$tables = is_array($tables) ? $tables : explode(',',$tables);
		}
		
		//cycle through
		foreach($tables as $table)
		{
			$result = mysql_query('SELECT * FROM '.$table);
			$num_fields = mysql_num_fields($result);
			
			$return.= 'DROP TABLE '.$table.';';
			$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
			$return.= "\n\n".$row2[1].";\n\n";
			
			for ($i = 0; $i < $num_fields; $i++) 
			{
				while($row = mysql_fetch_row($result))
				{
					$return.= 'INSERT INTO '.$table.' VALUES(';
					for($j=0; $j<$num_fields; $j++) 
					{
						$row[$j] = addslashes($row[$j]);
						$row[$j] = ereg_replace("\n","\\n",$row[$j]);
						if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
						if ($j<($num_fields-1)) { $return.= ','; }
					}
					$return.= ");\n";
				}
			}
			$return.="\n\n\n";
		}
		
		// Save file as .SQL
		$handle = fopen('../backups/db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql','w+');
		fwrite($handle,$return);
		fclose($handle);
		mysql_close($link);
	}
	
	// Send E-mail to Admin w/ .SQL attachment	
	$fileatt = "../backups/db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql"; // Path to the file
	$fileatt_type = "application/sql"; // File Type
	$fileatt_name = "db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql"; // Filename that will be used for the file as the attachment 

	$to = "citrustwistkits@yahoo.com"; 
	$subject = "Citrus Twist - Database Backup";
	$message = "You have a new registered user that may require your attention.";		
	$client_ip = $_SERVER['REMOTE_ADDR'];
	$headers = "First Name: $First \nLast Name: $Last \nUsername: $Username \nPhone #: $Phone \nIP: $client_ip\r\n " ;
	$file = fopen($fileatt,'rb');
	$data = fread($file,filesize($fileatt));
	fclose($file); 
	
	$data = chunk_split(base64_encode($data));
	$email_message .= "--{$mime_boundary}\n" .
	"Content-Type: {$fileatt_type};\n" .
	" name=\"{$fileatt_name}\"\n" .
	"Content-Disposition: attachment;\n" .
	" filename=\"{$fileatt_name}\"\n" .
	"Content-Transfer-Encoding: base64\n\n" .
	$data .= "\n\n" .
	"--{$mime_boundary}--\n"; 
		
	$sent = mail($to, $subject, $headers, $message) ; 
	if ($sent)
	{
		
		// Write information to log file.
		$errorlog = "../logs/log_file.txt";
		$myerror = "Database backup successfully completed. Email sent to Admin";
		$error = $ErrorDate.'  '.$time .': '.$myerror."\r\n";
		if (file_exists($errorlog)) {
			file_put_contents($errorlog, $error, FILE_APPEND | LOCK_EX);
		}
		else
		{
			file_put_contents($errorlog, $error);
		}

	}
	else
	{
		// Write information to log file.
		$errorlog = "../logs/log_file.txt";
		$myerror = "Database backup successfully completed. Could NOT send Email to Admin";
		$error = $ErrorDate.'  '.$time .': '.$myerror."\r\n";
		if (file_exists($errorlog)) {
			file_put_contents($errorlog, $error, FILE_APPEND | LOCK_EX);
		}
		else
		{
			file_put_contents($errorlog, $error);
		}
	}
	
	echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../index.php?dbbackup=true">';	
	
?>