<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script src="http://code.jquery.com/jquery-latest.js"></script>

	<link href="../styles.css" rel="stylesheet" type="text/css" />
	<link href="../favicon.ico" rel="shortcut icon" />
	<title>WebDesign - Client Invoicing System v2.0 by Digital Dreams</title>
<script type="text/javascript">
	$(document).ready(function(){
		$("form").submit(function(){
			$.post('my_ajax.php', {val1: "EditClient", val2: this.id}, function (response) {
			  var url = "../index.php";    
				$(location).attr('href',url);
			});
		});
	});
</script>
</head>
<?php 
	session_start();
	$path = 'auth.php';
	require_once($path);
	if (!isset($_SESSION['USER']) || (!isset($_SESSION['KEYCODE'])))
	{
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../logout.php?unauthorized=true">';
	}		
?> 

<body>
<div class="wrapper">
	<div class="header"></div>
	<div class="login">
		<p>&nbsp;</p>
		
		<div id="menu">
			<ul>
				<li><a href="../index.php">HOME</a></li>
				<?php
					if (isset($_SESSION['USER']))
					{
						echo '<li><a href="addclient.php">ADD CLIENT</a></li>';
        				echo '<li><a href="viewclients.php" class="current">CLIENT LIST</a></li>';
						echo '<li><a href="addproject.php">ADD PROJECT</a></li>';
        				echo '<li><a href="viewprojects.php">PROJECT LIST</a></li>';
						echo '<li><a href="prepinvoice.php">INVOICE</a></li>';
						echo '<li><a href="export.php">EXPORT</a></li>';
						
					}
				?>
				<li><a href="../logout.php">LOGOUT</a></li>
			</ul>
		</div>
        <div class="clearit"></div>
		<br /><hr><br />
		<?php
			$my_t=getdate(date("U"));
			$month = sprintf("%02s", $my_t[mon]);
			$Date =("$month-$my_t[mday]-$my_t[year]");
			
			// Connect to DB
			$con = mysql_connect($database_host,$username,$password);
	        if (!$con)
    	    {
        	  die('Could not connect: ' . mysql_error());
	        }

    	    mysql_select_db($database_name, $con);
			$sql ="SELECT * FROM clients WHERE RecNo = ". $_SESSION['ACTION'];
        	$result = mysql_query($sql);
			if ($result)
			{
				while($row = mysql_fetch_array($result))
        		{
					$_SESSION['IDENT'] = $row['RecNo'];
					echo '<img src="../images/content_header.png" >';
					echo '<h3>Record No: '. $_SESSION['ACTION'] .'</h3><br /><br />';      
					echo '<form action="my_ajax.php" width="400" height="400"  method="post" enctype="multipart/form-data" name="edit" >';
					echo '<p><label>Client: </label><input name="ClientName" type="text" size="25" maxlngth="80" placeholder="Name of Client/Company" value="'. $row['ClientName'] .'" />';
					echo '<label>Client ID: </label><input name="ClientID" type="text" size="25" maxlngth="80" placeholder="Unique Identifier" value="'. $row['ClientID'] .'" /></p><br />';
					echo '<p><label>Street: </label><input name="Address" type="text" size="30" maxlngth="80" placeholder="Address Including Apt or Suite" value="'. $row['Address'] .'"  />';
					echo '<label>City: </label><input name="City" type="text" size="25" maxlngth="80" value="'. $row['City'] .'"  />';
					echo '<label>State :</label><select name="State" size="1">';
					echo '<option>AL</option><option>AK</option><option>AZ</option><option>AR</option><option selected>CA</option><option>CO</option><option>CT</option><option>DE</option><option>FL</option><option>GA</option><option>HI</option><option>ID</option><option>IL</option><option>IN</option><option>IA</option><option>KS</option><option>KY</option><option>LA</option><option>ME</option><option>MD</option><option>MA</option><option>MI</option><option>MN</option><option>MS</option><option>MO</option><option>MT</option><option>NE</option><option>NV</option><option>NH</option><option>NJ</option><option>NM</option><option>NY</option><option>NC</option><option>ND</option><option>OH</option><option>OK</option><option>OR</option><option>PA</option><option>RI</option><option>SC</option><option>SD</option><option>TN</option><option>TX</option><option>UT</option><option>VT</option><option>VA</option><option>WA</option><option>WV</option><option>WI</option><option>WY</option><option>Other</option>';
					echo '</select>';
					echo '<label>Zip: </label><input name="Zip" type="text" size="12" maxlngth="10" placeholder="#####-####" value="'. $row['Zip'] .'"  /></p><br />';
					echo '<p><label>Work Ph: </label><input name="WorkPh" type="text" size="12" maxlngth="12" placeholder="###-###-####" value="'. $row['WorkPh'] .'"  />';
					echo '<label>Cell Ph: </label><input name="CellPh" type="text" size="12" maxlngth="12" placeholder="###-###-####" value="'. $row['CellPh'] .'"  />';
					echo '<label>Fax Ph: </label><input name="FaxPh" type="text" size="12" maxlngth="12" placeholder="###-###-####" value="'. $row['FaxPh'] .'"  /></p><br />';
					echo '<p><label>POC: </label><input name="POC" type="text" size="25" maxlngth="80" placeholder="Point of Contact" value="'. $row['POC'] .'"  />';
					echo '<label>Email: </label><input name="Email" type="text" size="25" maxlngth="80" placeholder="Email Address" value="'. $row['Email'] .'"  /></p><br />';
					echo '<p><label>FTPUrl: </label><input name="FTPUrl" type="text" size="25" maxlngth="80" placeholder="FTP URL" value="'. $row['FTPUrl'] .'"  /></p><br />';
					echo '<p><label>FTPUser: </label><input name="FTPUser" type="text" size="25" maxlngth="80" placeholder="FTP Username" value="'. $row['FTPUser'] .'"  /></p><br />';
					echo '<p><label>FTPPass: </label><input name="FTPPass" type="text" size="25" maxlngth="80" placeholder="FTP Password" value="'. $row['FTPPass'] .'"  /></p><br />';					
					echo '<p><label>Notes: </label><textarea name="Notes" cols="50" rows="5" placeholder="Additional Notes" value="'. $row['Notes'] .'" ></textarea></p><br />';
					echo '<input name="Type" type="hidden" value="EditClient">';
					echo '<p>&nbsp;</p>';
					echo '<label>Delete Client: </label><input name="Delete" type="checkbox" value="1"><br />';
					echo '<input name="submit" type="submit" value="Submit" />';
					echo '<input name="reset" type="reset" value="Reset" />';
					echo '</form>';
					echo '<img src="../images/content_footer.png" >';
				}
			}
		?>
        
	</div>
</div>
<div class="clearit"></div>
<div class="footer">
	<div id="footer1">
		<h4>Copyright &copy; 2013. <a href="#">All Rights Reserved.</a></h4>
	</div>
	<div id="footer2">
		<h4>Designed by: <a href="http://www.digitaldream-designs.com" target="_blank">Digital Dreams</a></h4>
	</div>
	
</div>
</body></html>
