<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script src="http://code.jquery.com/jquery-latest.js"></script>

	<link href="../styles.css" rel="stylesheet" type="text/css" />
	<link href="../favicon.ico" rel="shortcut icon" />
	<title>WebDesign - Client Invoicing System v2.0 by Digital Dreams</title>	
<script type="text/javascript">
	$(document).ready(function(){
		$("form").submit(function(){
			$.post('my_ajax.php', {val1: "EditPro", val2: this.id}, function (response) {
			  var url = "viewprojects.php";    
				$(location).attr('href',url);
			});
		});
	});
</script>
</head>
<?php 
	session_start();
	$path = 'auth.php';
	require_once($path);
	if (!isset($_SESSION['USER']) || (!isset($_SESSION['KEYCODE'])))
	{
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../logout.php?unauthorized=true">';
	}
?> 

<body>
<div class="wrapper">
	<div class="header"></div>
	<div class="login">
		<p>&nbsp;</p>
		
		<div id="menu">
			<ul>
				<li><a href="../index.php">HOME</a></li>
				<?php
					if (isset($_SESSION['USER']))
					{
						echo '<li><a href="addclient.php">ADD CLIENT</a></li>';
        				echo '<li><a href="viewclients.php">CLIENT LIST</a></li>';
						echo '<li><a href="addproject.php">ADD PROJECT</a></li>';
        				echo '<li><a href="viewprojects.php" class="current">PROJECT LIST</a></li>';
						echo '<li><a href="prepinvoice.php">INVOICE</a></li>';
						echo '<li><a href="export.php">EXPORT</a></li>';
						
					}
				?>
				<li><a href="../logout.php">LOGOUT</a></li>
			</ul>
		</div>
        <div class="clearit"></div>
		<br /><hr><br />
		<?php
			$my_t=getdate(date("U"));
			$month = sprintf("%02s", $my_t[mon]);
			$Date =("$month-$my_t[mday]-$my_t[year]");
		?>
		<img src="../images/content_header.png" >
		<form action="my_ajax.php" width="400" height="400"  method="post" enctype="multipart/form-data" name="add">
            <?php
				$con = mysql_connect($database_host,$username,$password);
				if (!$con)
				{
				  die('Could not connect: ' . mysql_error());
				}
				
				mysql_select_db($database_name, $con);
				$sql = 'SELECT * FROM invoices WHERE RecNo="'.$_SESSION['IDENT'].'"';
				$result = mysql_query($sql);
				while($row = mysql_fetch_array($result))
       			{
					echo '<p><label>Client: </label>'. $row['ClientName'].'</p><br />';
					echo '<p><label>Job Type: </label><select name="Job">';
	                if ($row['Job'] == "Website Design")
						echo '<option selected>Website Design</option>';
					else
						echo '<option>Website Design</option>';
					if ($row['Job'] == "Site Modifications")
						echo '<option selected>Site Modifications</option>';
					else
						echo '<option>Site Modifications</option>';
					if ($row['Job'] == "Site Management")
						echo '<option selected>Site Management</option>';
					else
						echo '<option>Site Management</option>';
					if ($row['Job'] == "Hosting Services")
						echo '<option selected>Hosting Services</option>';
					else
						echo '<option>Hosting Services</option>';
					if ($row['Job'] == "SEO")
						echo '<option selected>SEO</option>';
					else
						echo '<option>SEO</option>';
                    if ($row['Job'] == "Analytics")
						echo '<option selected>Analytics</option>';
					else
						echo '<option>Analytics</option>';
	                if ($row['Job'] == "PHP Scripting")
						echo '<option selected>PHP Scripting</option>';
					else
						echo '<option>PHP Scripting</option>';
					if ($row['Job'] == "ASP Scripting")
						echo '<option selected>ASP Scripting</option>';
					else
						echo '<option>ASP Scripting</option>';
					if ($row['Job'] == "Business Card")
						echo '<option selected>Business Card</option>';
					else
						echo '<option>Business Card</option>';
					if ($row['Job'] == "Brochure")
						echo '<option selected>Brochure</option>';
					else
						echo '<option>Brochure</option>';
					if ($row['Job'] == "Other")
						echo '<option selected>Other</option>';
					else
						echo '<option>Other</option>';						
                    echo '</select>';
					echo '<label>Project Type: </label><select name="Project">';
					if ($row['Project'] == "Basic")
						echo '<option selected>Basic</option>';
					else
						echo '<option>Basic</option>';
					if ($row['Project'] == "Responsive")
						echo '<option selected>Responsive</option>';
					else
						echo '<option>Responsive</option>';
					if ($row['Project'] == "Joomla")
						echo '<option selected>Joomla</option>';
					else
						echo '<option>Joomla</option>';
					if ($row['Project'] == "WordPress")
						echo '<option selected>WordPress</option>';
					else
						echo '<option>WordPress</option>';
					if ($row['Project'] == "Custom CMS")
						echo '<option selected>Custom CMS</option>';
					else
						echo '<option>Custom CMS</option>';
					if ($row['Project'] == "eCommerce")
						echo '<option selected>eCommerce</option>';
					else
						echo '<option>eCommerce</option>';
					if ($row['Project'] == "Other")
						echo '<option selected>Other</option>';
					else
						echo '<option>Other</option>';	
                    echo '</select>';					
					echo '<label>Status: </label><select name="Status">';
					if ($row['Status'] == "Open")
                    	echo '<option selected>Open</option>';
					else
						echo '<option>Open</option>';
					if ($row['Status'] == "Testing Phase")
                    	echo '<option selected>Testing Phase</option>';
					else
						echo '<option>Testing Phase</option>';
					if ($row['Status'] == "Om Hold")
                    	echo '<option selected>On Hold</option>';
					else
						echo '<option>On Hold</option>';
					if ($row['Status'] == "Waiting on Client")
                    	echo '<option selected>Waiting on Client</option>';
					else
						echo '<option>Waiting on Client</option>';
					if ($row['Status'] == "Invoice Submitted")
                    	echo '<option selected>Invoice Submitted</option>';
					else
						echo '<option>Invoice Submitted</option>';
					if ($row['Status'] == "Closed")						
                    	echo '<option selected>Closed</option>';
					else
						echo '<option>Closed</option>';
                   	echo '</select></p>';
					echo '<p><label>Contract: </label>';
					if ($row['Contract'] == "1")
						echo '<input name="Contract" type="checkbox" checked >';
					else
						echo '<input name="Contract" type="checkbox" >';
					echo '<label>Due Date: </label><input name="DueDate" type="number" size="14" placeholder="##/##/####" value="'. $row['DueDate']. '" /></p><br />';     
					echo '<h4>LINE ITEMS - </h4>';
					echo '<p><label>QTY: # </label><input name="Qty1" type="number" size="10" placeholder="0" value="'. $row['Qty1']. '" />';
					echo '<label>Job Desc: </label><input name="JobDesc1" type="text" size="60" maxlength="80" placeholder="Details of Line Item" value="'. $row['JobDesc1']. '" >';
					echo '<label>Unit Price: $ </label><input name="UnitPrice1" type="number" size="14" placeholder="0.00" value="'. $row['UnitPrice1']. '" /></p><br />';
					
					echo '<p><label>QTY: # </label><input name="Qty2" type="number" size="10" placeholder="0" value="'. $row['Qty2']. '" />';
					echo '<label>Job Desc: </label><input name="JobDesc2" type="text" size="60" maxlength="80" placeholder="Details of Line Item" value="'. $row['JobDesc2']. '">';
					echo '<label>Unit Price: $ </label><input name="UnitPrice2" type="number" size="14" placeholder="0.00" value="'. $row['UnitPrice2']. '" /></p><br />';
					
					echo '<p><label>QTY: # </label><input name="Qty3" type="number" size="10" placeholder="0" value="'. $row['Qty3']. '" />';
					echo '<label>Job Desc: </label><input name="JobDesc3" type="text" size="60" maxlength="80" placeholder="Details of Line Item" value="'. $row['JobDesc3']. '">';
					echo '<label>Unit Price: $ </label><input name="UnitPrice3" type="number" size="14" placeholder="0.00" value="'. $row['UnitPrice3']. '" /></p><br />';
					
					echo '<p><label>QTY: # </label><input name="Qty4" type="number" size="10" placeholder="0" value="'. $row['Qty4']. '" />';
					echo '<label>Job Desc: </label><input name="JobDesc4" type="text" size="60" maxlength="80" placeholder="Details of Line Item" value="'. $row['JobDesc4']. '">';
					echo '<label>Unit Price: $ </label><input name="UnitPrice4" type="number" size="14" placeholder="0.00" value="'. $row['UnitPrice4']. '" /></p><br />';
					
					echo '<p><label>QTY: # </label><input name="Qty5" type="number" size="10" placeholder="0" value="'. $row['Qty5']. '" />';
					echo '<label>Job Desc: </label><input name="JobDesc5" type="text" size="60" maxlength="80" placeholder="Details of Line Item" value="'. $row['JobDesc5']. '">';
					echo '<label>Unit Price: $ </label><input name="UnitPrice5" type="number" size="14" placeholder="0.00" value="'. $row['UnitPrice5']. '" /></p><br />';
					
					echo '<p><label>QTY: # </label><input name="Qty6" type="number" size="10" placeholder="0" value="'. $row['Qty6']. '" />';
					echo '<label>Job Desc: </label><input name="JobDesc6" type="text" size="60" maxlength="80" placeholder="Details of Line Item" value="'. $row['JobDesc6']. '">';
					echo '<label>Unit Price: $ </label><input name="UnitPrice6" type="number" size="14" placeholder="0.00" value="'. $row['UnitPrice6']. '" /></p><br />';
							   
					echo '<p><label>Amount Quoted: $ </label><input name="AmountQUOTE" type="number" size="14" placeholder="0.00" value="'. $row['AmountQUOTE']. '" />';
					echo '<label>Amount Received: $ </label><input name="AmountREC" type="number" size="14" placeholder="0.00" value="'. $row['AmountREC']. '"  /></p><br />';
					
					echo '<p><label>Other Url: </label><input name="OtherUrl" type="text" size="25" maxlength="80" placeholder="Additional URL" value="'. $row['OtherUrl']. '" />';
					echo '<label>Username: </label><input name="OtherUser" type="text" size="25" maxlength="80" value="'. $row['OtherUser']. '" /> ';
					echo '<label>Password: </label><input name="OtherPass" type="text" size="25" maxlength="80" value="'. $row['OtherPass']. '" /></p><br />';
					echo '<p><label>Notes: </label><textarea name="Notes" cols="50" rows="5" placeholder="Additional Notes" value="'. $row['Notes']. '" ></textarea></p><br />';
				}
				mysql_close($con);					
			?>
        	<input name="Type" type="hidden" value="EditProject">
			<p>&nbsp;</p>
            <label>Delete Project: </label><input name="Delete" type="checkbox" value="1"><br />
			<input name="submit" type="submit" value="Submit" />
			<input name="reset" type="reset" value="Reset" />		
        </form>
        <img src="../images/content_footer.png" >
	</div>
</div>
<div class="clearit"></div>
<div class="footer">
	<div id="footer1">
		<h4>Copyright &copy; 2013. <a href="#">All Rights Reserved.</a></h4>
	</div>
	<div id="footer2">
		<h4>Designed by: <a href="http://www.digitaldream-designs.com" target="_blank">Digital Dreams</a></h4>
	</div>
	
</div>
</body></html>
