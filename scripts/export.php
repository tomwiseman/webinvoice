<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script src="http://code.jquery.com/jquery-latest.js"></script>

	<link href="../styles.css" rel="stylesheet" type="text/css" />
	<link href="../favicon.ico" rel="shortcut icon" />
	<title>WebDesign - Client Invoicing System v2.0 by Digital Dreams</title>	
</head>
<?php 
	session_start();
	$path = 'auth.php';
	require_once($path);
	if (!isset($_SESSION['USER']) || (!isset($_SESSION['KEYCODE'])))
	{
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../logout.php?unauthorized=true">';
	}
	 
	// Connect and query the database for the users
	$con = mysql_connect($database_host,$username,$password);
	if (!$con)
	  {
	  die('Could not connect: ' . mysql_error());
	  }
	mysql_select_db($database_name, $con);

	$result = mysql_query("SELECT * FROM clients ORDER BY RecNo asc");
	 
	// Pick a filename and destination directory for the file
	// Remember that the folder where you want to write the file has to be writable
	$filename = "../csv/client_list.csv";
	 
	// Delete old file if exists	
	if (file_exists($filename)) { unlink ($filename); }
	// Actually create the file
	// The w+ parameter will wipe out and overwrite any existing file with the same name
	$handle = fopen($filename, 'w+');
	 
	// Write the spreadsheet column titles / labels
	fputcsv($handle, array('ClientName', 'ClientID', 'Address', 'City', 'State', 'Zip', 'WorkPh', 'CellPh', 'FaxPh', 'POC', 'Email', 'Notes'));
	 
	// Write all the user records to the spreadsheet
	//foreach($result as $row)
	while($row = mysql_fetch_array($result))
	{
		fputcsv($handle, array($row['ClientName'], $row['ClientID'], $row['Address'], $row['City'], $row['State'], $row['Zip'], $row['WorkPh'], $row['$CellPh'], $row['FaxPh'], $row['POC'], $row['Email'], $row['Notes']));
	}
	 
	// Finish writing the file
	fclose($handle);
	mysql_close($con);
	echo '<script type="text/javascript">';
		echo 'alert("Data exported to /csv/client_list.csv");';
	echo '</script>';
	echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../index.php?export=yes">';
?>

