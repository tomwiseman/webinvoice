<!DOCTYPE html>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css" />
<link href="../favicon.ico" rel="shortcut icon" />
<title>WebDesign - Client Invoicing System v2.0 by Digital Dreams</title>

<script src="http://code.jquery.com/jquery-latest.js"></script>
</head>
<?php
	session_start();	
	$path = 'auth.php';
	require_once($path);
	
	$Username = check_input($_POST['Username'], "Enter Username");
	$Password = check_input($_POST['Password'], "Enter Password");
	
	$time = date("\a\\t g.i a", time());
	$my_t=getdate(date("U"));
	$month = sprintf("%02s", $my_t[mon]);
	$day = sprintf("%02s", $my_t[mday]);
	$ErrorDate =("$month-$day-$my_t[year]");
	
	// Connect to DB
	$con = mysql_connect($database_host,$username,$password);
	if (!$con)
	{
		die('<p>Could not connect: ' . mysql_error() . '<p>');
	}
	mysql_select_db($database_name, $con);
	$sql = "SELECT * FROM users WHERE UserName='" . mysql_real_escape_string($Username) ."' AND Password = '" . mysql_real_escape_string($Password) . "'";	
	$result = mysql_query($sql);
	if ($result)
	{
		while($row = mysql_fetch_array($result))
        {
			$_SESSION['USER'] = $Username;
			//$hash = crypt($_SESSION['KEYCODE'],$salt);
			//if (crypt($_SESSION['KEYCODE'], $hash) == $hashkey)
			if ($row['KeyCode'] !='')
			{
				$_SESSION['KEYCODE'] = $row['KeyCode'];
				echo '<script type="text/javascript">';
					echo 'alert("Key Verified.\n'.$_SESSION['KEYCODE'].'");';
				echo '</script>';
				mysql_close($con);
				$KeyCode = $_SESSION['KEYCODE'];
				// Write information to log file.
				$errorlog = "../logs/log_file.txt";
				$myerror = "User logged into system as: $Username, with key: $KeyCode";
				$error = $ErrorDate.'  '.$time .': '.$myerror."\r\n";
				if (file_exists($errorlog)) {
					file_put_contents($errorlog, $error, FILE_APPEND | LOCK_EX);
				}
				else
				{
					file_put_contents($errorlog, $error);
				}				
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../index.php?loggedin=true">';
				exit;
			}
			else
			{
				echo '<script type="text/javascript">';
					echo 'alert("Unable to verify key!\nPlease contact the system administrator.");';
				echo '</script>';
				mysql_close($con);
				// Write information to log file.
				$errorlog = "../logs/log_file.txt";
				$myerror = "Could not verify KeyCode for $Username.";
				$error = $ErrorDate.'  '.$time .': '.$myerror."\r\n";
				if (file_exists($errorlog)) {
					file_put_contents($errorlog, $error, FILE_APPEND | LOCK_EX);
				}
				else
				{
					file_put_contents($errorlog, $error);
				}
				// Unset all of the session variables.
				$_SESSION = array();
				// Destroy Session
				session_destroy();
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../index.php?loggedin=false">';
				exit;
			}
		}
		
	}
	else
	{
		// Write information to log file.
		$errorlog = "../logs/log_file.txt";
		$myerror = "Failed login attempt for $Username";
		$error = $ErrorDate.'  '.$time .': '.$myerror."\r\n";
		if (file_exists($errorlog)) {
			file_put_contents($errorlog, $error, FILE_APPEND | LOCK_EX);
		}
		else
		{
			file_put_contents($errorlog, $error);
		}
		
		// Unset all of the session variables.
		$_SESSION = array();
		// Destroy Session
		session_destroy();
		mysql_close($con);
		echo '<script type="text/javascript">';
			echo 'alert("Username/Password not found!");';
		echo '</script>';
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../index.php?loggedin=false">';	
	}
	
	
	function check_input($data, $problem='')
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		if ($problem && strlen($data) == 0)
		{
			show_error($problem);
		}
		return $data;
	}
	
	function show_error($myError)
	{
	?>
		<html>
		<body>
	
		<b>Please correct the following error:</b><br />
		<?php
        	echo '<script type="text/javascript">';
				echo 'alert("ERROR: '.$myError.'");';
			echo '</script>';
			echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../index.php?loggedin=false">';
        ?>
	
		</body>
		</html>
	<?php
	exit();
	}
?> 
