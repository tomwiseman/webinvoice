<!DOCTYPE html>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css" />
<link href="favicon.ico" rel="shortcut icon" />
<title>WebDesign - Client Invoicing System v2.0 by Digital Dreams</title>

<script src="http://code.jquery.com/jquery-latest.js"></script>
		
</head>
<?php
   	session_start();
   	$path = 'auth.php';
	require_once($path);
	if (!isset($_SESSION['USER']) || (!isset($_SESSION['KEYCODE'])))
	{
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../logout.php?unauthorized=true">';
	}
   	$value1 = $_POST['val1'];
   	$value2 = $_POST['val2'];

   	if ($value1 == "AddClient")
		$_SESSION['ACTION'] = "AddClient";
   	elseif ($value1 == "AddProject")
		$_SESSION['ACTION'] = "AddProject";
	elseif ($value1 == "EditProject")
	{
		$_SESSION['ACTION'] = "EditProject";		
		$_SESSION['IDENT'] = $value2;		
	}
	elseif ($value1 == "PrepInvoice")
		$_SESSION['IDENT'] = $value2;
	else
		$_SESSION['ACTION'] = $value2;

	echo "PHPVersion: ". phpversion() ."<br />";
	
	// Pull values from HTML form
	$ClientName = ucwords($_POST['ClientName']);
	$ClientID = $_POST['ClientID'];
	$Address = $_POST['Address'];
	$City = ucwords($_POST['City']);
	$State = ucwords($_POST['State']);
	$Zip = $_POST['Zip'];
	$WorkPh = $_POST['WorkPh'];
	$CellPh = $_POST['CellPh'];
	$FaxPh = $_POST['FaxPh'];
	$POC = ucwords($_POST['POC']);
	$Email = $_POST['Email'];
	$Notes = $_POST['Notes'];
	$FTPUrl = $_POST['FTPUrl'];
	$FTPUser = $_POST['FTPUser'];
	$FTPPass = $_POST['FTPPass'];
	
	$Job = $_POST['Job'];
	$Project = $_POST['Project'];
	$Status = $_POST['Status'];
	$Invoice = $_POST['Invoice'];
	$ProjectStart = $_POST['ProjectStart'];
	if ($_POST['Contract'])
		$Contract = 1;
	else
		$Contract = 0;
	$DueDate = $_POST['DueDate'];
	$Qty1 = $_POST['Qty1'];
	if ($Qty1 == '')
		$Qty1 = '0';
	$UnitPrice1 = $_POST['UnitPrice1'];
	if ($UnitPrice1 =='')
		$UnitPrice1 = '0.00';
	$JobDesc1 = $_POST['JobDesc1'];
	$Qty2 = $_POST['Qty2'];
	if ($Qty2 == '')
		$Qty2 = '0';
	$UnitPrice2 = $_POST['UnitPrice2'];
	if ($UnitPrice2 =='')
		$UnitPrice2 = '0.00';
	$JobDesc2 = $_POST['JobDesc2'];
	$Qty3 = $_POST['Qty3'];
	if ($Qty3 == '')
		$Qty3 = '0';
	$UnitPrice3 = $_POST['UnitPrice3'];
	if ($UnitPrice3 =='')
		$UnitPrice3 = '0.00';
	$JobDesc3 = $_POST['JobDesc3'];
	$Qty4 = $_POST['Qty4'];
	if ($Qty4 == '')
		$Qty4 = '0';
	$UnitPrice4 = $_POST['UnitPrice4'];
	if ($UnitPrice4 =='')
		$UnitPrice4 = '0.00';
	$JobDesc4 = $_POST['JobDesc4'];
	$Qty5 = $_POST['Qty5'];
	if ($Qty5 == '')
		$Qty5 = '0';
	$UnitPrice5 = $_POST['UnitPrice5'];
	if ($UnitPrice5 =='')
		$UnitPrice5 = '0.00';
	$JobDesc5 = $_POST['JobDesc5'];	
	$Qty6 = $_POST['Qty6'];
	if ($Qty6 == '')
		$Qty6 = '0';
	$UnitPrice6 = $_POST['UnitPrice6'];
	if ($UnitPrice6 =='')
		$UnitPrice6 = '0.00';
	$JobDesc6 = $_POST['JobDesc6'];	
		
	$AmountQUOTE = $_POST['AmountQUOTE'];
	if ($AmountQUOTE =='')
		$AmountQuote = '0.00';
	$AmountREC = $_POST['AmountREC'];
	if ($AmountREC =='')
		$AmountREC = '0.00';
	
	$OtherUrl = $_POST['OtherUrl'];
	$OtherUser = $_POST['OtherUser'];
	$OtherPass = $_POST['OtherPass'];
	$Notes = $_POST['Notes'];
	
	$KeyCode = $_SESSION['KEYCODE']; // Unique Invoice Customer Identifier.	
   
   	// Add Client
	if (isset($_SESSION['USER']) && ($_POST['Type']=="AddClient"))
   	{
		
		echo '<h3>Add Client</h3>';
		// Connect to DB & Insert values
		$con = mysql_connect($database_host,$username,$password);
		if (!$con)
	  	{
	  		die('Could not connect: ' . mysql_error());
	  	}
		mysql_select_db($database_name, $con);
		$result = mysql_query("INSERT INTO clients (ClientName, ClientID, KeyCode, Address, City, State, Zip, WorkPh, CellPh, FaxPh, POC, Email, Notes)
VALUES ('$ClientName', '$ClientID', '$KeyCode', '$Address', '$City', '$State', '$Zip', '$WorkPh', '$CellPh', '$FaxPh', '$POC', '$Email', '$Notes')");
		if ($result)
		{
			echo '<script type="text/javascript">';
				echo 'alert("1 Record Added.");';
			echo '</script>';
			mysql_close($con);	
			echo '<META HTTP-EQUIV="Refresh" Content="0; URL=addclient.php?addclient=true">';
		}
		else
		{
			echo '<script type="text/javascript">';
				echo 'alert("Error No# '. mysql_error().'\nFailed to update database!");';
			echo '</script>';
			mysql_close($con);			
			echo '<META HTTP-EQUIV="Refresh" Content="0; URL=addclient.php?addclient=false">';
		}		
   	}

   	// Edit Client
	if (isset($_SESSION['USER']) && ($_POST['Type']=="EditClient"))   
	{  
		// Connect to DB & Update values Di2.NMYZ38kyU
		$con = mysql_connect($database_host,$username,$password);
		if (!$con)
		{
			die('Could not connect: ' . mysql_error());
		}
		mysql_select_db($database_name, $con);
		if ($_POST['Delete'])
		{
			echo '<h3>Delete Client</h3>';
			$sql ="DELETE FROM clients WHERE RecNo =". $_SESSION['IDENT'];
			$result = mysql_query($sql);
			if ($result)
			{
				echo '<script type="text/javascript">';
					echo 'alert("1 Record Deleted.");';
				echo '</script>';
				mysql_close($con);	
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=viewclients.php?deleteclient=true">';
			}
			else
			{
				echo '<script type="text/javascript">';
					echo 'alert("Error No# '. mysql_error().'\nFailed to update database!");';
				echo '</script>';
				mysql_close($con);	
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=viewclients.php?deleteclient=false">';
			}	
		}
		else
		{
			echo '<h3>Edit Client</h3>';			 
			$sql="UPDATE clients SET ClientName='$ClientName', ClientID='$ClientID', Address='$Address', City='$City', State='$State', Zip='$Zip', WorkPh='$WorhPh', CellPh='$CellPh', FaxPh='$FaxPh', POC='$POC', FTPUrl='$FTPUrl', FTPUser='$FTPUser', FTPPass='$FTPPass', Email ='$Email', Notes='$Notes' WHERE RecNo =". $_SESSION['IDENT'];
			$result = mysql_query($sql);
			if ($result)
			{
				echo '<script type="text/javascript">';
					echo 'alert("1 Record Modified.");';
				echo '</script>';
				mysql_close($con);	
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=viewclients.php?editclient=true">';
			}
			else
			{
				echo '<script type="text/javascript">';
					echo 'alert("Error No# '. mysql_error().'\nFailed to update database!");';
				echo '</script>';
				mysql_close($con);	
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=editclient.php?editclient=false">';
			}	
		}
	}

   	// Add Project	
   	if (isset($_SESSION['USER']) && ($_POST['Type']=="AddProject"))
   	{
		echo '<h3>Add Project</h3>';
		// Connect to DB & Insert values
		$con = mysql_connect($database_host,$username,$password);
		if (!$con)
	  	{
	  		die('Could not connect: ' . mysql_error());
	  	}
		mysql_select_db($database_name, $con);
		$sql = ("INSERT INTO invoices (ClientName, KeyCode, Job, Project, Status, Invoice, ProjectStart, Contract, DueDate, Qty1, JobDesc1, UnitPrice1, Qty2, JobDesc2, UnitPrice2, Qty3, JobDesc3, UnitPrice3, Qty4, JobDesc4, UnitPrice4, Qty5, JobDesc5, UnitPrice5, Qty6, JobDesc6, UnitPrice6, AmountQUOTE, AmountREC, OtherUrl, OtherUser, OtherPass, Notes) 
		VALUES
		('$ClientName', '$KeyCode', '$Job', '$Project', '$Status', '$Invoice', '$ProjectStart', '$Contract', '$DueDate', '$Qty1', '$JobDesc1', '$UnitPrice1', '$Qty2', '$JobDesc2', '$UnitPrice2', '$Qty3', '$JobDesc3', '$UnitPrice3', '$Qty4', '$JobDesc4', '$UnitPrice4', '$Qty5', '$JobDesc5', '$UnitPrice5', '$Qty6', '$JobDesc6', '$UnitPrice6', '$AmountQUOTE', '$AmountREC', '$OtherUrl', '$OtherUser', '$OtherPass', '$Notes')");
		if (mysql_query($sql,$con))
		{
			echo '<script type="text/javascript">';
				echo 'alert("1 Record Added.");';
			echo '</script>';
			mysql_close($con);	
			echo '<META HTTP-EQUIV="Refresh" Content="0; URL=addproject.php?addproject=true">';	
		}
		else
		{
			echo '<script type="text/javascript">';
				echo 'alert("Error No# '. mysql_error().'\nFailed to update database!");';
			echo '</script>';
			mysql_close($con);
			echo '<META HTTP-EQUIV="Refresh" Content="0; URL=addproject.php?addproject=false">';
		}

   	}

   	// Edit Project	
	if (isset($_SESSION['USER']) && ($_POST['Type']=="EditProject"))
   	{
		echo '<h3>Edit Project: '. $_SESSION['IDENT'] . '</h3>';
		// Connect to DB & Update values
		$con = mysql_connect($database_host,$username,$password);
		if (!$con)
	  	{
	  		die('Could not connect: ' . mysql_error());
	  	}
		mysql_select_db($database_name, $con);
		if ($_POST['Delete'])
		{
			echo '<h3>Delete Project</h3>';
			$sql ="DELETE FROM invoices WHERE RecNo =". $_SESSION['IDENT'];
			$result = mysql_query($sql);
			if ($result)
			{
				echo '<script type="text/javascript">';
					echo 'alert("1 Record Deleted.");';
				echo '</script>';
				mysql_close($con);	
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=viewprojects.php?deleteproject=true">';
			}
			else
			{
				echo '<script type="text/javascript">';
					echo 'alert("Error No# '. mysql_error().'\nFailed to update database!");';
				echo '</script>';
				mysql_close($con);	
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=viewprojects.php?deleteproject=false">';
			}
		}
		else
		{
	   		$sql="UPDATE invoices SET Job='$Job', Project='$Project', Status='$Status', Contract='$Contract', DueDate='$DueDate', Qty1='$Qty1', JobDesc1='$JobDesc1', UnitPrice1='$UnitPrice1', Qty2='$Qty2', JobDesc2='$JobDesc2', UnitPrice2='$UnitPrice2', Qty3='$Qty3', JobDesc3='$JobDesc3', UnitPrice3='$UnitPrice3', Qty4='$Qty4', JobDesc4='$JobDesc4', UnitPrice4='$UnitPrice4', Qty5='$Qty5', JobDesc5='$JobDesc5', UnitPrice5='$UnitPrice5', Qty6='$Qty6', JobDesc6='$JobDesc6', UnitPrice6='$UnitPrice6', AmountQUOTE='$AmountQUOTE', AmountREC='$AmountREC', OtherUrl='$OtherUrl', OtherUser='$OtherUser', OtherPass='$OtherPass', Notes='$Notes' WHERE RecNo =". $_SESSION['IDENT'];
			$result = mysql_query($sql);
			if ($result)
			{
				echo '<script type="text/javascript">';
					echo 'alert("1 Record Modified.");';
				echo '</script>';
				mysql_close($con);	
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=viewprojects.php?editproject=true">';
			}
			else
			{
				echo '<script type="text/javascript">';
					echo 'alert("Error No# '. mysql_error().'\nFailed to update database!");';
				echo '</script>';
				mysql_close($con);
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=viewprojects.php?editproject=false">';
			}
		}
	}
?>