<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php 
	session_start();
	$path = 'auth.php';
	require_once($path);
	if (!isset($_SESSION['USER']))
	{
		header("Location: ./scripts/logout.php");
	}		
?>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css" />
<link href="favicon.ico" rel="shortcut icon" />
<title>Digital Dreams - Client Invoicing System v1.3</title>

</head>
<body>
<div class="wrapper">
	<div class="header"></div>
	<div class="login">
		<p>&nbsp;</p>
		
		<div id="menu">
			<ul>
				<li><a href="index.php">HOME</a></li>
				<?php
					if (isset($_SESSION['USER']))
					{
						echo '<li><a href="add.php">ADD INVOICE</a></li>';
						echo '<li><a href="view.php">VIEW INVs</a></li>';
						echo '<li><a href="delete.php" class="current">DEL INVOICE</a></li>';
						echo '<li><a href="export.php">EXPORT</a></li>';
						echo '<li><a href="invoice.php">INVOICE</a></li>';
					}
				?>
				<li><a href="../logout.php">LOGOUT</a></li>
			</ul>
		</div>
		<p>&nbsp;</p>
		<p>&nbsp;</p> 
	
		<?php
        $con = mysql_connect($database_host,$username,$password);
        if (!$con)
          {
          die('Could not connect: ' . mysql_error());
          }
        
        mysql_select_db($database_name, $con);
        
        $result = mysql_query("SELECT * FROM invoices");
        echo '<table width="100%" border="1">';
		
        while($row = mysql_fetch_array($result))
          {
			echo '<tr id="highlight1"><td>CustName: ' . $row['CustName'] . '</td>';
			echo '<td>CustID: ' . $row['CustID'] . '</td>';
			echo '<td>Job: ' . $row['Job'] . '</td></tr>';
			echo '<tr><td>Invoice: ' . $row['Invoice'] . '</td>';
			echo '<td>Invoice Date: ' . $row['DOA'] . '</td>';
			echo '<td>ID: ' . $row['RecNO'] . '</td></tr>';
          }
        echo '</table>';
        mysql_close($con);
        ?>
		<form action="./scripts/deleteinvoice.php" width="400" height="400"  method="post" enctype="multipart/form-data" name="delete">
			
			<label>ID# to DELETE: </label><input name="id" type="text" size="5" maxlength="5" id="idnumber" /><br />
			<p>&nbsp;</p>		
			<input name="submit" type="submit" value="Submit" />
			<input name="reset" type="reset" value="Reset" />
		</form>
	</div>
</div>
<div class="clearit"></div>
<div class="footer">
	<div id="footer1">
		<h4>Copyright &copy; 2013. All Rights Reserved.</h4>
	</div>
	<div id="footer2">
		<h4>Designed by: <a href="http://www.digitaldream-designs.com" target="_blank">Digital Dreams</a></h4>
	</div>
	
</div>
</body></html>
