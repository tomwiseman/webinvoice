<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php 
	session_start();
	$path = 'auth.php';
	require_once($path);
	if (!isset($_SESSION['USER']))
	{
		header("Location: ./scripts/logout.php");
	}		
?>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css" />
<link href="favicon.ico" rel="shortcut icon" />
<title>Digital Dreams - Client Invoicing System v1.3</title>


</head>
<body>
<div class="wrapper">
	<div class="header"></div>
	<div class="login">
		<p>&nbsp;</p>
		
		<div id="menu">
			<ul>
				<li><a href="index.php">HOME</a></li>
				<?php
					if (isset($_SESSION['USER']))
					{
						echo '<li><a href="add.php">ADD INVOICE</a></li>';
						echo '<li><a href="view.php" class="current">VIEW INVs</a></li>';
						echo '<li><a href="delete.php">DEL INVOICE</a></li>';
						echo '<li><a href="export.php">EXPORT</a></li>';
						echo '<li><a href="invoice.php">INVOICE</a></li>';
					}
				?>
				<li><a href="../logout.php">LOGOUT</a></li>
			</ul>
		</div>
		<p>&nbsp;</p>    
		<?php
		$Client = $_SESSION['CLIENT'];
		//$ID = $_POST['id'];
		
		// Connect to DB
		$con = mysql_connect($database_host,$username,$password);
		if (!$con)
		  {
		  die('Could not connect: ' . mysql_error());
		  }
		echo "Database Connection Established","<br>";
		echo 'ID#: ' . $Client . '<br /><br />';
		mysql_select_db($database_name, $con);
        
        $result = mysql_query("SELECT * FROM invoices WHERE RecNo =" .$Client);
		while($row = mysql_fetch_array($result))
          {
			echo '<form action="./scripts/editinvoice.php" width="400" height="400"  method="post" enctype="multipart/form-data" name="add">';
			echo '<p><label>Client: </label><input name="CustName" type="text" size="40" value="' . $row['CustName'] . '" />';
			echo '<label>Client ID: </label><input name="CustID" type="text" size="40" value="' . $row['CustID'] . '" /></p><br />';
			echo '<label>Job Type: </label><select name="Job">';
            	echo '<option>Website Design</option>';
                echo '<option>Site Modifications</option>';
                echo '<option>Site Management</option>';
				echo '<option>Hosting Services</option>';
                echo '<option>SEO</option>';
                echo '<option>Analytics</option>';
                echo '<option>PHP Scripting</option>';
                echo '<option>ASP Scripting</option>';
                echo '<option>Business Card</option>';
                echo '<option>Brochure</option>';
				echo '<option>Other</option>';       
                echo '</select>';
			echo '<label>Project Type: </label><select name="Project">';
            	echo '<option>Basic</option>';
                echo '<option>Responsive</option>';
                echo '<option>Joomla</option>';
                echo '<option>WordPress</option>';
                echo '<option>Custom CMS</option>';
				echo '<option>eCommerce</option>';
                echo '<option>Other</option>';
                echo '</select>';
				
			echo '<label>Status: </label><select name="Status">';
            	echo '<option>Open</option>';
                echo '<option>Testing Phase</option>';
                echo '<option>On Hold</option>';
                echo '<option>Waiting on Client</option>';
                echo '<option>Invoice Submitted</option>';
				echo '<option>Closed</option>';
                echo '</select>';
			echo '<p><label>Invoice #: </label><input name="Invoice" type="number" size="11" value="' . $row['Invoice'] . '" />';
			echo '<label>Date of Invoice: </label><input name="DOA" type="date" size="12" value="' . $row['DOA'] . '" /></p><br />';
			echo '<p><label>Amount Invoiced: $ </label><input name="AmountINV" type="number" size="7" value="' . $row['AmountINV'] . '" />';
			echo '<label>Amount Received: $ </label><input name="AmountREC" type="number" size="7" value="' . $row['AmountREC'] . '" /></p><br />';
			
			echo '<p><label>Qty: </label><input name="Qty" type="text" size="25" value="' . $row['Qty'] . '" />';
			echo '<label>Job Desc: </label><input name="JobDesc" type="text" size="25" value="' . $row['JobDesc'] . '" />';
			echo '<label>Unit Price: </label><input name="UnitPrice" type="text" size="25" value="' . $row['UnitPrice'] . '" /></p><br />';
			
			echo '<p><label>Contact Name: </label><input name="ClientName" type="text" size="25" value="' . $row['ClientName'] . '" /></p>';
			echo '<p><label>Mailing Address: </label><input name="MailingAdd" type="text" size="25" value="' . $row['MailingAdd'] . '" />';
			echo '<label>City: </label><input name="City" type="text" size="25" value="' . $row['MailingCity'] . '" /></p>';
			echo '<p><label>State: </label><input name="State" type="text" size="25" value="' . $row['MailingSt'] . '" />';
			echo '<label>Zip: </label><input name="Zip" type="text" size="25" value="' . $row['MailingZip'] . '" /></p>';
			echo '<p><label>Contact Email: </label><input name="ClientEmail" type="text" size="25" value="' . $row['ClientEmail'] . '" />';
			echo '<label>Contact Phone: </label><input name="ClientPhone" type="text" size="25" value="' . $row['ClientPhone'] . '" /></p><br />';
			
			echo '<p><label>FTP Url: </label><input name="FTPUrl" type="text" size="40" value="' . $row['FTPUrl'] . '" />';
			echo '<label>FTP Username: </label><input name="FTPUser" type="text" size="25" value="' . $row['FTPUser'] . '" />';
			echo '<label>FTP Password: </label><input name="FTPPass" type="text" size="25" value="' . $row['FTPPass'] . '" /></p><br />';
			echo '<p><label>Other Url: </label><input name="OtherUrl" type="text" size="40" value="' . $row['OtherUrl'] . '" />';
			echo '<label>Other Username: </label><input name="OtherUser" type="text" size="25" value="' . $row['OtherUser'] . '" />';
			echo '<label>Other Password: </label><input name="OtherPass" type="text" size="25" value="' . $row['OtherPass'] . '" /></p><br />';
			echo '<p><label>Notes: </label><input name="Notes" type="text" size="40" value="' . $row['Notes'] . '" /></p>';
			echo '<p>&nbsp;</p>';
			echo '<input name="id" type="hidden" value="' . $ID . '">';
			echo '<input name="submit" type="submit" value="Submit" />';
			echo '<input name="reset" type="reset" value="Reset" />';
			echo '</form>';
		  }
		// Close DB
		mysql_close($con);
		?>
	</div>
	<div class="clearit"></div>
	<div class="footer">
		<div id="footer1">
			<h4>Copyright &copy; 2013. All Rights Reserved.</h4>
		</div>
		<div id="footer2">
			<h4>Designed by: <a href="http://www.digitaldream-designs.com" target="_blank">Digital Dreams</a></h4>
		</div>
		
	</div>
</div>
</body></html>