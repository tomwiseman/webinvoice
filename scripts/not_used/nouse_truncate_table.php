<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="refresh" content="3; URL=../view.php">
<?php 
	session_start();
?>

<head>
<link href="../styles.css" rel="stylesheet" type="text/css" />
<link href="favicon.ico" rel="shortcut icon" />
<title>Digital Dreams - Client Invoicing System v1.3</title>

</head>
<body>
<div class="wrapper">
	<div class="logo">
	</div>
	<div class="login">
		<p>&nbsp;</p>
		
		<div id="menu">
			<ul>
				<li><a href="index.php">HOME</a></li>
				<li><a href="add.php">ADD INVOICE</a></li>
				<li><a href="edit.php">EDIT INVOICE</a></li>
				<li><a href="view.php">VIEW ALL</a></li>
				<li><a href="delete.php">DEL INVOICE</a></li>
				<li><a href="truncate_table.php" class="current">TRUNCATE DB</a></li>
			</ul>
		</div>
		<p>&nbsp;</p>
		<p>&nbsp;</p> 
	
		<?php
        $con = mysql_connect("localhost","root","frpj35a");
        if (!$con)
          {
          die('Could not connect: ' . mysql_error());
          }
        
        mysql_select_db($database_name, $con);
        
        $result = mysql_query("SELECT * FROM invoices");
        echo '<table width="100%" border="1">';
		
        while($row = mysql_fetch_array($result))
          {
			echo '<tr id="highlight1"><td>CustName: ' . $row['CustName'] . '</td>';
			echo '<td>CustID: ' . $row['CustID'] . '</td>';
			echo '<td>Job: ' . $row['Job'] . '</td></tr>';
			echo '<tr><td>Invoice: ' . $row['Invoice'] . '</td>';
			echo '<td>Invoice Date: ' . $row['DOA'] . '</td>';
			echo '<td>ID: ' . $row['RecNO'] . '</td></tr>';
          }
        echo '</table>';
        mysql_close($con);
        ?>
		<form action="truncate.php" width="400" height="400"  method="post" enctype="multipart/form-data" name="delete">
			<p>&nbsp;</p>
			<p id="red">After clicking the button below, all invoices within the database will be deleted. There is no recovery mechanism once you click the button.</p>
			<p>&nbsp;</p>
			<p>Are you sure you wish to remove all invoice entries?</p>
			<input name="submit" type="submit" value="YES" />
		</form>
	</div>
</div>
<div class="clearit"></div>
<div class="footer">
	<div id="footer1">
		<h4>Copyright &copy; 2013. All Rights Reserved.</h4>
	</div>
	<div id="footer2">
		<h4>Designed by: <a href="http://www.digitaldream-designs.com" target="_blank">Digital Dreams</a></h4>
	</div>
	
</div>
</body></html>
