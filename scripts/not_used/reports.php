<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php 
	session_start();
	$path = './scripts/auth.php';
	require_once($path)
?> 

<head>
<link href="styles.css" rel="stylesheet" type="text/css" />
<link href="favicon.ico" rel="shortcut icon" />

<title>Digital Dreams - Client Invoicing System v1.3</title>

</head>
<body>
<div class="wrapper">
	<div class="header"></div>
	<div class="login">
		<p>&nbsp;</p>
		
		<div id="menu">
			<ul>
				<li><a href="index.php">HOME</a></li>
				<?php
					if (isset($_SESSION['USER']))
					{
						echo '<li><a href="./scripts/addclient.php">ADD CLIENT</a></li>';
        				echo '<li><a href="./scripts/viewclients.php">CLIENT LIST</a></li>';
						echo '<li><a href="./scripts/addproject.php">ADD PROJECT</a></li>';
        				echo '<li><a href="./scripts/viewprojects.php">PROJECT LIST</a></li>';
						echo '<li><a href="./scripts/prepinvoice.php">INVOICES</a></li>';
						echo '<li><a href="./scripts/export.php" class="current">REPORTS</a></li>';
					}
				?>
				<li><a href="logout.php">LOGOUT</a></li>
			</ul>
		</div>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p id="red">This application and the associated database were designed  by DIGITAL DREAMS.</p>
		<p id="red">*Any use of this application outside the intended organization or copying of files without express written consent from the author will be considered a copyright infringement. Penalties may include fines up to $150,000, and/or imprisonment.</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>To purchase a personalized copy of this application, please submit a request by visiting the <a href="http://www.digitaldream-designs.com/contact.html" target="_blank">Digital Dreams Contact page</a> or e-mailing the author <a href="mailto:TomMWiseman@digitaldream-designs.com">Tom Wiseman</a></p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<img src="images/dd_logo.jpg" alt="Digital Dreams Logo" align="left" hspace="15">
      	<h3>User Login</h3>
      	<form action="./scripts/loginuser.php" width="425" height="150"  method="post" enctype="multipart/form-data" name="loginuser" id="loginuser">        
        	<label>Username: </label><input name="Username" type="text" size="15"><br /><br />
          	<label>Password: </label><input name="Password" type="password" size="10">
          	<br /><br />
          	<input name="submit" type="submit" value="Submit" />
          	<input name="reset" type="reset" value="Reset" />
      	</form>
		
		
	</div>
</div>
<div class="clearit"></div>
<div class="footer">
	<div id="footer1">
		<h4>Copyright &copy; 2013, All Rights Reserved.</h4>
	</div>
	<div id="footer2">
		<h4>Designed by: <a href="http://www.digitaldream-designs.com" target="_blank">Digital Dreams</a></h4>
	</div>
	
</div>
</body></html>
