<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<link href="../styles.css" rel="stylesheet" type="text/css" />
<link href="../favicon.ico" rel="shortcut icon" />
<title>WebDesign - Client Invoicing System v2.0 by Digital Dreams</title>

<script src="http://code.jquery.com/jquery-latest.js"></script>

</head>
<?php
	session_start();
	if (!isset($_SESSION['USER']) || (!isset($_SESSION['KEYCODE'])))
	{
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../logout.php?unauthorized=true">';
	}
	else
	{
		$path = 'auth.php';
		require_once($path);
		require('fpdf.php');
		ob_end_clean();	
	
		$pdf = new FPDF();
		$pdf->SetAuthor("Digital Dreams - web design studio", False);
		$pdf->SetCreator("Digital Dreams", False);
		$pdf->SetTitle("Client Invoice", False);
		$pdf->SetSubject("Client Invoice", False);
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetFont('Arial','',10);
		$pdf->SetFillColor(50,10,255);
				
		$my_t=getdate(date("U"));
		$month = sprintf("%02s", $my_t[mon]);
		$Date =("$month-$my_t[mday]-$my_t[year]");
		
		$ID = $_SESSION['IDENT'];
		
		$con = mysql_connect($database_host,$username,$password);
		if (!$con)
		{
		  die('Could not connect: ' . mysql_error());
		}		
		mysql_select_db($database_name, $con);
		$result = mysql_query("SELECT * FROM invoices WHERE RecNo =" .$_SESSION['IDENT']);
		while($row = mysql_fetch_array($result))
		{
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(150,5,'Digital Dreams',0,0);
			$pdf->Cell(40,5,'Invoice # '. $row['Invoice'],1,1, 'R');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(150,5,'"From Ordinary to Extraordinary"',0,0);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(40,5,'Date: '. $Date,1,1, 'R');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(35,5,'1117 Mariposa Dr. Yuba City, CA 95991',0,1);
			$pdf->Cell(35,5,'PH: 530.990.0020',0,1);
			$pdf->Cell(35,5,'www.digitaldream-designs.com',0,1);
			$pdf->Ln(5);
			$pdf->Line(1,70,215,70);
			$pdf->Ln(5);
			$pdf->Cell(100,5,'TO: ' . $row['ClientName'],0,0);
			$pdf->SetTextColor(255,0,0);
			$pdf->Cell(90,5,'*Please make all checks payable to:',0,1, 'R');
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(100,5,'' . $row['CustName'],0,0);
			$pdf->SetTextColor(255,0,0);
			$pdf->Cell(90,5,'Tom Wiseman',0,1, 'R');
			
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(100,5,'' . $row['MailingAdd'],0,1);
			$pdf->Cell(100,5,'' . $row['MailingCity'],0,0);
			$pdf->Cell(100,5,'' . $row['MailingSt'],0,0);
			$pdf->Cell(100,5,'' . $row['MailingZip'],0,1);
			$pdf->Cell(100,5,'' . $row['ClientPhone'],0,1);
			$pdf->Cell(100,5,'Client ID: ' . $row['CustID'],0,1);
			
			$pdf->Ln(5);
			$pdf->Line(1,110,215,110);
			$pdf->Ln(5);
			
			$pdf->Cell(50,5,'SALES PERSON',1,0,'C');
			$pdf->Cell(50,5,'JOB',1,0,'C');
			$pdf->Cell(50,5,'PAYMENT TERMS',1,0,'C');
			$pdf->Cell(50,5,'DUE DATE',1,1,'C');
			$pdf->Cell(50,5,'Tom Wiseman',1,0,'C');
			$pdf->Cell(50,5, $row['Job'],1,0,'C');
			$pdf->Cell(50,5,'Due on receipt',1,0,'C');
			$pdf->Cell(50,5,'60 Days From Invoice',1,1,'C');
			$pdf->Ln(5);
			$pdf->Cell(50,5,'QTY',1,0,'C');
			$pdf->Cell(50,5,'DESCRIPTION',1,0,'C');
			$pdf->Cell(50,5,'UNIT PRICE',1,0,'C');
			$pdf->Cell(50,5,'LINE TOTAL',1,1,'C');
			if ($row['Qty'] == NULL)
				$pdf->Cell(50,5,'1',1,0,'C');
			else
				$pdf->Cell(50,5,$row['Qty'],1,0,'C');
			if ($row['JobDesc'] == NULL)
				$pdf->Cell(50,5,$row['Job'],1,0,'C');
			else
				$pdf->Cell(50,5,$row['JobDesc'],1,0,'C');
			if ($row['UnitPrice'] == NULL)
				$pdf->Cell(50,5,$row['AmountINV'],1,0,'C');
			else
				$pdf->Cell(50,5,sprintf ("%.2f",$row['UnitPrice']),1,0,'C');
			if ($row['Qty'] == NULL || $row['UnitPrice'] ==NULL)
				$LineTotal = sprintf ("%.2f",(1 * $row['AmountINV']));
			else
				$LineTotal = sprintf ("%.2f",($row['Qty'] * $row['UnitPrice']));
			$pdf->Cell(50,5,$LineTotal,1,0,'C');
			$pdf->Ln(10);
			
			$SubTotal = $LineTotal;
			$SalesTax = sprintf ("%.2f",($SubTotal * 0.075));
			$Total = sprintf ("%.2f",($SubTotal + $SalesTax));
			$pdf->SetX(110);
			$pdf->Cell(100,5,'SUBTOTAL: $'.$SubTotal,1,1, 'C');
			$pdf->SetX(110);
			$pdf->Cell(100,5,'SALES TAX: $'. $SalesTax ,1,1, 'C');
			$pdf->SetX(110);
			$pdf->Cell(100,5,'TOTAL: $'. $Total,1,1, 'C');
			$pdf->Ln(15);
			$pdf->SetTextColor(255,0,0);		
			$pdf->Cell(200,5,'THANK YOU FOR YOUR BUSINESS',0,1,'C');
			$pdf->SetTextColor(0,0,0);
			$pdf->Ln(15);
		}
		mysql_close($con);
		$pdf->Output();
	}

?>
