<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php 
	session_start();
	$path = 'auth.php';
	require_once($path);
	if (!isset($_SESSION['USER']) || (!isset($_SESSION['KEYCODE'])))
	{
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../logout.php?unauthorized=true">';
	}		
?>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css" />
<link href="../favicon.ico" rel="shortcut icon" />
<title>WebDesign - Client Invoicing System v2.0 by Digital Dreams</title>

<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('tr a').click(function(){
			$.post('my_ajax.php', {val1: "PrepInvoice", val2: this.id}, function (response) {
			  //alert (response);
			  var url = "pdfinvoice.php";    
				$(location).attr('href',url);
			});
		});
	});	
</script>

</head>
<body>
<div class="wrapper">
	<div class="header"></div>
	<div class="login">
		<p>&nbsp;</p>
		
		<div id="menu">
			<ul>
				<li><a href="../index.php">HOME</a></li>
				<?php
					if (isset($_SESSION['USER']))
					{
						echo '<li><a href="addclient.php">ADD CLIENT</a></li>';
        				echo '<li><a href="viewclients.php">CLIENT LIST</a></li>';
						echo '<li><a href="addproject.php">ADD PROJECT</a></li>';
        				echo '<li><a href="viewprojects.php">PROJECT LIST</a></li>';
						echo '<li><a href="prepinvoice.php" class="current">INVOICE</a></li>';
						echo '<li><a href="export.php">EXPORT</a></li>';
						
					}
				?>
				<li><a href="../logout.php">LOGOUT</a></li>
			</ul>
		</div>
		<div class="clearit"></div><br />
		
        <?php echo '<div class="UserIdent">User: <span>'. strtoupper($_SESSION['USER']) .'</span></div>'; ?>	
		<br /><hr><br />
		<?php
        $con = mysql_connect($database_host,$username,$password);
        if (!$con)
        {
        	die('Could not connect: ' . mysql_error());
        }        
        mysql_select_db($database_name, $con);        
        $result = mysql_query("SELECT * FROM invoices ORDER BY ClientName asc");
		echo '<img src="../images/content_header.png" >';
		echo '<center><h3>Select Client by Clicking Name</h3></center><br />';
        echo '<table width="100%" border="1">';		
        while($row = mysql_fetch_array($result))
          {
			echo '<tr id="highlight1"><td><a href="#" id="'. $row['RecNo'] .'">'. $row['ClientName'] . '</a></td>';
			echo '<td>' . $row['RecNo'] . '</td>';
			echo '<td>Invoice: ' . $row['Invoice'] .  '</td></tr>';
			echo '<tr><td>ID: ' . $row['RecNo'] . '</td>';
			echo '<td>Quoted $' . $row['AmountQUOTE'] . '</td>';
			echo '<td>Received: $' . $row['AmountREC'] . '</td></tr>';
          }
        echo '</table>';
		echo '<img src="../images/content_footer.png" >';
        mysql_close($con);
        ?>
	</div>
</div>
<div class="clearit"></div>
<div class="footer">
	<div id="footer1">
		<h4>Copyright &copy; 2013. <a href="#">All Rights Reserved.</a></h4>
	</div>
	<div id="footer2">
		<h4>Designed by: <a href="http://www.digitaldream-designs.com" target="_blank">Digital Dreams</a></h4>
	</div>
	
</div>
</body></html>
