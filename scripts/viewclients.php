<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<?php 
	session_start();
	$path = 'auth.php';
	require_once($path);
	if (!isset($_SESSION['USER']) || (!isset($_SESSION['KEYCODE'])))
	{
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../logout.php?unauthorized=true">';
	}	
?>

<head>
<link href="../styles.css" rel="stylesheet" type="text/css" />
<link href="../favicon.ico" rel="shortcut icon" />
<title>WebDesign - Client Invoicing System v2.0 by Digital Dreams</title>

<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('table a').click(function(){
			$.post('my_ajax.php', {val1: "Client", val2: this.id}, function (response) {
			  //alert (response);
			  var url = "editclient.php";    
				$(location).attr('href',url);
			});
		});
	});	
</script>
</head>
<body>
<div class="wrapper">
	<div class="header"></div>
	<div class="login">
		<p>&nbsp;</p>
		
		<div id="menu">
			<ul>
				<li><a href="../index.php">HOME</a></li>
				<?php
					if (isset($_SESSION['USER']))
					{
        				echo '<li><a href="viewclients.php" class="current">CLIENT LIST</a></li>';
        				echo '<li><a href="viewprojects.php">PROJECT LIST</a></li>';
						echo '<li><a href="prepinvoice.php">INVOICE</a></li>';
						echo '<li><a href="export.php">EXPORT</a></li>';
					}
				?>
				<li><a href="../logout.php">LOGOUT</a></li>
			</ul>
		</div>
        <div id="submenu">
			<ul>
				<?php
					if (isset($_SESSION['USER']))
					{
						echo '<li><a href="addclient.php">ADD CLIENT</a></li>';
					}
				?>
				
			</ul>
		</div>
		<div class="clearit"></div><br />
		
        <?php echo '<div class="UserIdent">User: <span>'. strtoupper($_SESSION['USER']) .'</span></div>'; ?>		
        
        <br /><hr><br />
		<?php
        $con = mysql_connect($database_host,$username,$password);
        if (!$con)
        {
          die('Could not connect: ' . mysql_error());
        }
        $KeyCode = $_SESSION['KEYCODE'];
        mysql_select_db($database_name, $con);
		$sql ="SELECT * FROM clients WHERE KeyCode='$KeyCode' ORDER By ClientName asc";
        $result = mysql_query($sql);
		echo '<img src="../images/content_header.png" >';
		echo '<table width="100%" border="0">';		
        while($row = mysql_fetch_array($result))
        {
			$clients++;
			$totproj += $row['NumProjects'];
			echo '<tr id="highlight1"><td><a href="#" id="'. $row['RecNo']. '">' . $row['ClientName'] . '</a></td>';
			echo '<td>ClientID: ' . $row['ClientID'] . '</td>';
			echo '<td>Total Projects: ' . $row['NumProjects'] . '</td></tr>';
			echo '<tr><td>'. $row['POC'] .'</td>';
			echo '<td>'. $row['Email'] .'</td></tr>';
			
			echo '<tr><td>Work #: '. $row['WorkPh'] .'</td>';
			echo '<td>Cell #: '. $row['CellPh'] .'</td>';
			echo '<td>Fax #: '. $row['FaxPh'] .'</td></tr>';
			
			echo '<tr><td>'. $row['Address'] .'</td>';
			echo '<td>'. $row['City'] .'</td>';
			echo '<td>'. $row['State'] .'</td></tr>';			
			
			echo '<tr><td>'. $row['FTPUrl'] .'</td>';
			echo '<td>'. $row['FTPUser'] .'</td>';
			echo '<td>'. $row['FTPPass'] .'</td></tr>';
						
			echo '<tr><td colspan="3">Notes: ' . $row['Notes'] . '</td></tr>';
			echo '<tr><td colspan="3"></td></tr>';
          }
        echo '</table>';
		echo '<img src="../images/content_footer.png" >';
		echo '<p>&nbsp</p>';
		echo '<p>Total Clients: ' . $clients;
		echo '<p>Total Projects: ' . $totproj;
		
        mysql_close($con);
        ?>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
	</div>
</div>
<div class="clearit"></div>
<div class="footer">
	<div id="footer1">
		<h4>Copyright &copy; 2013. <a href="#">All Rights Reserved.</a></h4>
	</div>
	<div id="footer2">
		<h4>Designed by: <a href="http://www.digitaldream-designs.com" target="_blank">Digital Dreams</a></h4>
	</div>
	
</div>
</body></html>
