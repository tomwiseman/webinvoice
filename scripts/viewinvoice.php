<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<?php 
	session_start();
	$path = 'auth.php';
	require_once($path);
	if (!isset($_SESSION['USER']) || (!isset($_SESSION['KEYCODE'])))
	{
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../logout.php?unauthorized=true">';
	}	
?>

<head>
<link href="../styles.css" rel="stylesheet" type="text/css" />
<link href="favicon.ico" rel="shortcut icon" />
<title>WebDesign - Client Invoicing System v2.0 by Digital Dreams</title>

<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('a').click(function(){
			// alert (this.id);
			// var Designer ="Designer";
			$.post('./scripts/my_ajax.php', {val1: "Client", val2: this.id}, function (response) {
			// $.post('designerbio.php', {val1: "Designer", val2: this.id});
			  //alert (response);
			  var url = "edit.php";    
				$(location).attr('href',url);
			});
		});
	});	
</script>
</head>
<body>
<div class="wrapper">
	<div class="header"></div>
	<div class="login">
		<p>&nbsp;</p>
		
		<div id="menu">
			<ul>
				<li><a href="index.php">HOME</a></li>
				<?php
					if (isset($_SESSION['USER']))
					{
						echo '<li><a href="add.php">ADD INVOICE</a></li>';
						echo '<li><a href="view.php" class="current">VIEW INVs</a></li>';
						echo '<li><a href="delete.php">DEL INVOICE</a></li>';
						echo '<li><a href="export.php">EXPORT</a></li>';
						echo '<li><a href="invoice.php">INVOICE</a></li>';
					}
				?>
				<li><a href="../logout.php">LOGOUT</a></li>
			</ul>
		</div>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<?php
        $con = mysql_connect($database_host,$username,$password);
        if (!$con)
        {
          die('Could not connect: ' . mysql_error());
        }
        $KeyCode = $_SESSION['KEYCODE'];
        mysql_select_db($database_name, $con);        
        $result = mysql_query("SELECT * FROM invoices WHERE KeyCode ='$KeyCode' ORDER BY RecNo desc");	
		echo '<table width="100%" border="1">';
		
        while($row = mysql_fetch_array($result))
          {
			$invoices++;
			echo '<tr id="highlight1"><td><a href="#" id="'. $row['RecNO']. '">' . $row['CustName'] . '</a></td>';
			echo '<td>Invoice: ' . $row['Invoice'] . '</td>';
			echo '<td>Job: ' . $row['Job'] . '</td>';
			if ($row['Status'] <> "Closed")
				{
				echo '<td id="highlight2">Status: ' . $row['Status'] . '</td></tr>';
				}
			else
				{
				echo '<td>Status: ' . $row['Status'] . '</td></tr>';
				}
			echo '<tr><td>' . $row['CustID']. '</td>';
			echo '<td>Invoice Date: ' . $row['DOA'] . '</td>';
			echo '<td>Amount INV $' . $row['AmountINV'] . '</td>';
			$TotInv = $TotInv + $row['AmountINV'];
			if ($row['AmountINV'] > 0 && $row['AmountREC'] < 1)
				{
				echo '<td id="highlight5">Amount REC $' . $row['AmountREC'] . '</td></tr>';
				}
			else
				{
				echo '<td>Amount REC $' . $row['AmountREC'] . '</td></tr>';
				}
			$TotRec = $TotRec + $row['AmountREC'];
			echo '<tr><td>FTP Url: ' . $row['FTPUrl'] . '</td>';
			echo '<td>FTP User: ' . $row['FTPUser'] . '</td>';
			echo '<td>FTP Pass: ' . $row['FTPPass'] . '</td></tr>';
			echo '<tr><td>Other Url: ' . $row['OtherUrl'] . '</td>';
			echo '<td>Other User: ' . $row['OtherUser'] . '</td>';
			echo '<td>Other Pass: ' . $row['OtherPass'] . '</td></tr>';
			echo '<tr><td>Contact: ' . $row['ClientName'] . '</td>';
			echo '<td>Address: ' . $row['MailingAdd'] . '</td>';
			echo '<td>E-mail: ' . $row['ClientEmail'] . '</td>';
			echo '<td>Phone: ' . $row['ClientPhone'] . '</td></tr>';
			echo '<tr><td colspan="4">Notes: ' . $row['Notes'] . '</td></tr>';
          }
        echo '</table>';
		echo '<p>&nbsp</p>';
		echo '<p>Total Invoices: ' . $invoices;
		echo '<p>Total Invoiced: $' . $TotInv;
		echo '<p>Total Received: $' . $TotRec;
		
        mysql_close($con);
        ?>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
	</div>
</div>
<div class="clearit"></div>
<div class="footer">
	<div id="footer1">
		<h4>Copyright &copy; 2013. <a href="#">All Rights Reserved.</a></h4>
	</div>
	<div id="footer2">
		<h4>Designed by: <a href="http://www.digitaldream-designs.com" target="_blank">Digital Dreams</a></h4>
	</div>
	
</div>
</body></html>
