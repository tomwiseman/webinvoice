<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<?php 
	session_start();
	$path = 'auth.php';
	require_once($path);
	if (!isset($_SESSION['USER']) || (!isset($_SESSION['KEYCODE'])))
	{
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../logout.php?unauthorized=true">';
	}	
?>

<head>
<link href="../styles.css" rel="stylesheet" type="text/css" />
<link href="../favicon.ico" rel="shortcut icon" />
<title>WebDesign - Client Invoicing System v2.0 by Digital Dreams</title>

<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.ClientName a').click(function(){
			$.post('my_ajax.php', {val1: "EditProject", val2: this.id}, function (response) {
			  //alert (response);
			  var url = "editproject.php";    
				$(location).attr('href',url);
			});
		});
	});	
</script>
</head>
<body>
<div class="wrapper">
	<div class="header"></div>
	<div class="login">
		<p>&nbsp;</p>		

		<div id="menu">
			<ul>
				<li><a href="../index.php">HOME</a></li>
				<?php
					if (isset($_SESSION['USER']))
					{
        				echo '<li><a href="viewclients.php">CLIENT LIST</a></li>';
        				echo '<li><a href="viewprojects.php" class="current">PROJECT LIST</a></li>';
						echo '<li><a href="prepinvoice.php">INVOICE</a></li>';
						echo '<li><a href="export.php">EXPORT</a></li>';
					}
				?>
				<li><a href="../logout.php">LOGOUT</a></li>
			</ul>
		</div>
        <div id="submenu">
			<ul>
				<?php
					if (isset($_SESSION['USER']))
					{
						echo '<li><a href="addproject.php">ADD PROJECT</a></li>';
					}
				?>
				
			</ul>
		</div>
        <div class="clearit"></div><br />
		
        <?php 
			if (isset($_SESSION['USER']))
				echo '<div class="UserIdent">User: <span>'. strtoupper($_SESSION['USER']) .'</span></div>';
		?>	
		<br /><hr><br />
        
		<?php
			$my_t=getdate(date("U"));
			$month = sprintf("%02s", $my_t[mon]);
			$Date =("$month-$my_t[mday]-$my_t[year]");
			$con = mysql_connect($database_host,$username,$password);
			if (!$con)
			{
			  die('Could not connect: ' . mysql_error());
			}
			$KeyCode = $_SESSION['KEYCODE'];
			mysql_select_db($database_name, $con);
			$sql ="SELECT * FROM invoices WHERE KeyCode = '$KeyCode' ORDER BY ClientName asc";
			$result = mysql_query($sql);
			if (!$result)
			{
				echo "Error: ". mysql_error();
				exit;	
			}
			while($row = mysql_fetch_array($result))
			{ 
				echo '<div class="ProjectView">';
					echo '<img src="../images/content_header.png" >';
					echo '<div class="ClientName">';
						echo '<a href="#" id="'. $row['RecNo'] .'" ><h3>'. $row['ClientName'] .'</h3></a>';
					echo '</div>';
					echo '<div class="Job">';
						echo '<p><strong>'. $row['Job'] .'</strong></p>';
					echo '</div>';
					echo '<div class="Job">';
						echo '<p><strong>Project Type: </strong>'. $row['Project'] .'</p>';
					echo '</div>';
					echo '<div class="Job">';
						if ($row['Status'] <> 'Closed')
							echo '<h5 id="red">Status: </strong>'. $row['Status'] .'</p>';
						else
							echo '<p><strong>Status: </strong>'. $row['Status'] .'</p>';
					echo '</div>';
					echo '<div class="clearit"></div>';
					echo '<div class="Project">';
						echo '<p><strong>Project Start: </strong>'. $row['ProjectStart'] .'</p>';
					echo '</div>';
					echo '<div class="Project">';
						echo '<p><strong>Project End: </strong>'. $row['ProjectEnd'] .'</p>';
					echo '</div>';
					echo '<div class="Project">';
						echo '<p><strong>Date: </strong>'. $row['DOI'] . '</p>';
					echo '</div>';
					echo '<div class="clearit"></div>';
					echo '<div class="Contract">';
						if ($row['Contract'] =='1')
							echo '<p><strong>Contract: </strong>Yes</p>';
						else
							echo '<p><strong>Contract: </strong>No</p>';
					echo '</div>';
					echo '<div class="Contract">';
						echo '<p><strong>Due Date: </strong>'. $row['DueDate'] .'</p>';
					echo '</div>';
					echo '<div class="clearit"></div>';
					echo '<div class="Invoice">';
						echo '<p><strong>Invoice #: </strong>'. $row['Invoice'] .'</p>';
					echo '</div>';
					echo '<div class="Invoice">';
						echo '<p><strong>Amount Quoted $ </strong>'. $row['AmountQUOTE'] .'</p>';
					echo '</div>';
					echo '<div class="Invoice">';
						echo '<p><strong>Amount REC $ </strong>'. $row['AmountREC'] .'</p>';
					echo '</div>';
					
					echo '<div class="clearit"></div>';
					echo '<div class="JobDesc">';
						echo '<p><strong>Qty: </strong>'. $row['Qty1'] .'</p>';
					echo '</div>';
					echo '<div class="JobDesc">';				
						echo '<p>'. $row['JobDesc1'] .'</p>';
					echo '</div>';
					echo '<div class="JobDesc">';
						echo '<p><strong>Unit Price $ </strong>'. $row['UnitPrice1'] .'</p>';
					echo '</div>';				
					echo '<div class="clearit"></div>';
					
					echo '<div class="JobDesc">';
						echo '<p><strong>Qty: </strong>'. $row['Qty2'] .'</p>';
					echo '</div>';
					echo '<div class="JobDesc">';				
						echo '<p>'. $row['JobDesc2'] .'</p>';
					echo '</div>';
					echo '<div class="JobDesc">';
						echo '<p><strong>Unit Price $ </strong>'. $row['UnitPrice2'] .'</p>';
					echo '</div>';				
					echo '<div class="clearit"></div>';
					
					echo '<div class="JobDesc">';
						echo '<p><strong>Qty: </strong>'. $row['Qty3'] .'</p>';
					echo '</div>';
					echo '<div class="JobDesc">';				
						echo '<p>'. $row['JobDesc3'] .'</p>';
					echo '</div>';
					echo '<div class="JobDesc">';
						echo '<p><strong>Unit Price $ </strong>'. $row['UnitPrice3'] .'</p>';
					echo '</div>';				
					echo '<div class="clearit"></div>';
					
					echo '<div class="JobDesc">';
						echo '<p><strong>Qty: </strong>'. $row['Qty4'] .'</p>';
					echo '</div>';
					echo '<div class="JobDesc">';				
						echo '<p>'. $row['JobDesc4'] .'</p>';
					echo '</div>';
					echo '<div class="JobDesc">';
						echo '<p><strong>Unit Price $ </strong>'. $row['UnitPrice4'] .'</p>';
					echo '</div>';				
					echo '<div class="clearit"></div>';
					
					echo '<div class="JobDesc">';
						echo '<p><strong>Qty: </strong>'. $row['Qty5'] .'</p>';
					echo '</div>';
					echo '<div class="JobDesc">';				
						echo '<p>'. $row['JobDesc5'] .'</p>';
					echo '</div>';
					echo '<div class="JobDesc">';
						echo '<p><strong>Unit Price $ </strong>'. $row['UnitPrice5'] .'</p>';
					echo '</div>';				
					echo '<div class="clearit"></div>';
					
					echo '<div class="JobDesc">';
						echo '<p><strong>Qty: </strong>'. $row['Qty6'] .'</p>';
					echo '</div>';
					echo '<div class="JobDesc">';				
						echo '<p>'. $row['JobDesc6'] .'</p>';
					echo '</div>';
					echo '<div class="JobDesc">';
						echo '<p><strong>Unit Price $ </strong>'. $row['UnitPrice6'] .'</p>';
					echo '</div>';				
					echo '<div class="clearit"></div>';
					
					echo '<div class="Url">';
						echo '<p><strong>Url: </strong>'. $row['OtherUrl'] .'</p>';
					echo '</div>';
					echo '<div class="Url">';
						echo '<p><strong>User: </strong>'. $row['OtherUser'] .'</p>';
					echo '</div>';
					echo '<div class="Url">';
						echo '<p><strong>Pass: </strong>'. $row['OtherPass'] .'</p>';
					echo '</div>';
					echo '<div class="clearit"></div>';
					echo '<div class="Notes">';
						echo '<p><strong>Notes: </strong>'. $row['Notes'] .'</p>';
					echo '</div>';
					echo '<div class="Total">';
						$TotINV = (($row['Qty1'] * $row['UnitPrice1'])+ ($row['Qty2'] * $row['UnitPrice2'])+ ($row['Qty3'] * $row['UnitPrice3'])+ ($row['Qty4'] * $row['UnitPrice4'])+ ($row['Qty5'] * $row['UnitPrice5']));
						echo '<p><strong>Total $ </strong>'. number_format($TotINV, 2, '.', '') .'</p>';
					echo '</div>';
				echo '</div>';
				echo '<div class="clearit"></div>';
				echo '<img src="../images/content_footer.png" >';
				echo '<br />';
			}
			mysql_close($con);
        ?>
	  	<p>&nbsp;</p>
		<p>&nbsp;</p>
	</div>
</div>
<div class="clearit"></div>
<div class="footer">
	<div id="footer1">
		<h4>Copyright &copy; 2013. <a href="#">All Rights Reserved.</a></h4>
	</div>
	<div id="footer2">
		<h4>Designed by: <a href="http://www.digitaldream-designs.com" target="_blank">Digital Dreams</a></h4>
	</div>
	
</div>
</body></html>
